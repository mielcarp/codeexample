<?php

namespace App\Command;

use App\Service\ImportXmlService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:import-xml',
    description: 'Import plikow xml z ftp-a',
)]
class ImportXmlCommand extends Command
{
    private ImportXmlService $importXmlService;

    public function __construct(ImportXmlService $importXmlService)
    {
        parent::__construct();
        $this->importXmlService = $importXmlService;
    }

    protected function configure(): void
    {
        $this
//            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
//            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $time_start = microtime(true);
        $io = new SymfonyStyle($input, $output);

        $this->importXmlService->import();

        $errors = $this->importXmlService->getErrors();

        if(!empty($errors)){
            $io->error(implode("\n", $errors));
            return Command::FAILURE;
        }

        $time_stop = microtime(true);

        $duration = $time_stop - $time_start;
        $io->info("Duration: " . $duration);

        $io->success('Import done!');


        return Command::SUCCESS;
    }
}
