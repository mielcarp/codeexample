<?php

namespace App\Service;

use App\Entity\Import;
use App\Entity\Product;
use App\Entity\Shop;
use Doctrine\ORM\EntityManagerInterface;
use DOMDocument;
use FtpClient\FtpClient;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\String\Slugger\SluggerInterface;
use XMLReader;
use Gregwar\Image\Image;

class ImportXmlService
{
    private $errors;

    private $xmlFiles;

    public function __construct(
        private EntityManagerInterface $em,
        private ParameterBagInterface $param,
        private SluggerInterface $slugger
    )
    {
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function import()
    {
        $importRepository = $this->em->getRepository(Import::class);

        $importInProgres = $importRepository->findOneBy(
            ['status' => 3]
        );

        if($importInProgres){
            $this->errors[] = "Import in progress";
            return;
        }

        /** @var Import $import */
        $import = $importRepository->findOneBy(
            ['status' => 0],
            ['createdAt' => 'ASC']
        );

        if (!$import) {
//            $this->downloadXml();
            $this->scanXmlDir();
        }


        if ($import) {
            $import->setStartAt(new \DateTimeImmutable());
            $import->setStatus(3);
            $this->em->flush();

            echo $import->getFile() . "\n";;
            $this->processXml($import->getFile());

            if (empty($this->errors)) {
                $import->setStatus(1);
            } else {
                $import->setStatus(2);
                $import->setErrorLog(implode(",\n", $this->errors));
            }

            $import->setEndAt(new \DateTimeImmutable());
//            $this->removeXml($import->getFile());
            $this->em->flush();
        }
    }

    public function removeXml($file){
        @unlink($this->param->get("app.import-xml.local-dir") . '/' . $file);
    }

    public function clearImages(){
        echo 'Skanuje foldery w poszukiwaniu plików' . "\n";

        $finder = new Finder();
        $finder->files()->in($this->param->get("app.import-xml.image-dir"));

        $countPack = 50;
        $i =  0;
        $packArr = [];
        foreach ($finder as $file) {
            if($file->getExtension() == 'jpg'){
                if($i <= $countPack){
                    $packArr[] = $file->getRelativePathname();
                    $i++;
                }else{
                    $productRepository = $this->em->getRepository(Product::class);
                    $products = $productRepository->findBy(
                        [
                            'imageProcessed' => $packArr
                        ]
                    );
                    if(!empty($products)){
                        /** @var Product $product */
                        foreach($products as $product){
                            foreach($packArr as $k=>$e){
                                if($product->getImageProcessed() == $e){
                                    unset($packArr[$k]);
                                }
                            }
                        }
                    }
                    foreach($packArr as $elem){
                        echo "Kasuje: " . $this->param->get("app.import-xml.image-dir") . '/' . $elem . "\n";
                        unlink($this->param->get("app.import-xml.image-dir") . '/' . $elem);
                    }
                    $packArr = [];
                    $i=0;
                    $this->em->clear();
                }


            }
        }
    }

    public function scanXmlDir(){

        echo 'Skanuje foldery w poszukiwaniu plików' . "\n";

        $finder = new Finder();
        $finder->files()->in($this->param->get("app.import-xml.local-dir"));

        foreach ($finder as $file) {
            if($file->getExtension() == 'xml'){
                $dateTimeImmutable = new \DateTimeImmutable();
                $mtimeImmutable = $dateTimeImmutable->setTimestamp($file->getMTime());

                $importRepository = $this->em->getRepository(Import::class);
                $fileFound = $importRepository->findOneBy([
                    'file' => $file->getRelativePathname(),
                    'fileMtime' => $mtimeImmutable
                ]);

                if($fileFound){
                    continue;
                }

                $importFile = new Import();
                $importFile->setFile($file->getRelativePathname());
                $importFile->setStatus(0);
                $importFile->setFileMtime($mtimeImmutable);
                $this->em->persist($importFile);
                $this->em->flush();
            }
        }
    }

    public function downloadXml()
    {
        try {
            $ftp = new FtpClient();
            echo "Laczenie z ftp\n";
            echo "host: " . $this->param->get("app.ftp.host") . "\n";
            echo "user: " . $this->param->get("app.ftp.user") . "\n";
            echo "pass: " . $this->param->get("app.ftp.pass") . "\n";
            $ftp->connect($this->param->get("app.ftp.host"));
            $ftp->login($this->param->get("app.ftp.user"), $this->param->get("app.ftp.pass"));
            $ftp->pasv(true);
            $files = $ftp->nlist(".");
            foreach ($files as $file) {
                if ($ftp->get($this->param->get("app.import-xml.local-dir") . '/' . $file, $file)) {
                    $importFile = new Import();
                    $importFile->setFile($file);
                    $importFile->setStatus(0);
                    $this->em->persist($importFile);
                    $this->em->flush();
//                    $this->xmlFiles[] = $file;
                    $ftp->delete($file);
                }
            }
            $ftp->close();
        } catch (\FtpException $e) {
            $this->errors[] = $e->getMessage();
        }
    }

    public function processXml($xmlFile)
    {
        try {

            echo 'Memory usage at start: ' . memory_get_usage() . "\n";
            $xmlArray = [];
            /** @var XMLReader $xml */
            $xml = XMLReader::open($this->param->get("app.import-xml.local-dir") . '/' . $xmlFile);

            $doc = new DOMDocument;

            //pobieram link do sklepu
            while ($xml->read()) {
                if ($xml->depth == 2 && $xml->name == 'link' && $xml->nodeType === \XmlReader::ELEMENT) {
                    list($node) = simplexml_import_dom($doc->importNode($xml->expand(), true));
                    $shopUrl = parse_url($node, PHP_URL_HOST);
                    break;
                }
            }

            $this->setToUpdate($shopUrl);

            $xml->next('item');

            $i = 0;
            while ($xml->name === 'item') {
                // either one should work
                //$node = new SimpleXMLElement($z->readOuterXML());
                $item = simplexml_import_dom($doc->importNode($xml->expand(), true));
                $googleBase = $item->children("http://base.google.com/ns/1.0");

                if($item->title == null || $item->title == ''){
                    continue;
                }

                $productRepository = $this->em->getRepository(Product::class);
                /** @var Product $product */
                $product = $productRepository->findOneBy([
                    'gtin' => $googleBase->gtin ?? '',
                    'mpn' => $googleBase->mpn ?? '',
                    'shop' => $shopUrl
                ]);

                $priceCurrency = explode(" ", $googleBase->price);
                list($category) = explode(" > ", $googleBase->google_product_category) ?? [];
//                $category = $googleBase->google_product_category;
                $slug = $this->slugger->slug($category, '_');

                if ($product) {
                    $product->setName($item->title);
                    $product->setGtin($googleBase->gtin ?? '');
                    $product->setPrice((int)($priceCurrency[0] * 100) ?? 0);
                    $product->setCurrency($priceCurrency[1] ?? 'PLN');
                    $product->setMpn($googleBase->mpn ?? '' );
                    $product->setUrl($item->link);
                    $product->setCategory($category ?? '');
                    $product->setCategorySlug($slug->lower()->toString() ?? '');
                    if($googleBase->image_link != $product->getImage()){
                        if($product->getImageProcessed() != ''){
                            if(is_file($this->param->get("app.import-xml.image-dir") . '/' . $product->getImageProcessed())){
                                unlink($this->param->get("app.import-xml.image-dir") . '/' . $product->getImageProcessed());
                            }
                        }
                        $product->setImage($googleBase->image_link);
                        $product->setImageProcessed('');
                        $product->setImageProcessedStatus(0);
                    }
                    $product->setShop($shopUrl);
                } else {
                    $product = new Product();
                    $product->setName($item->title);
                    $product->setGtin($googleBase->gtin ?? '');
                    $product->setPrice((int)($priceCurrency[0] * 100) ?? 0);
                    $product->setCurrency($priceCurrency[1] ?? 'PLN');
                    $product->setMpn($googleBase->mpn ?? '');
                    $product->setUrl($item->link);
                    $product->setImage($googleBase->image_link);
                    $product->setCategory($category ?? '');
                    $product->setCategorySlug($slug->lower()->toString() ?? '');
                    $product->setImageProcessed('');
                    $product->setShop($shopUrl);
                    $this->em->persist($product);
                }

                $this->em->flush();
                $this->em->clear(Product::class);
                $this->em->detach($product);


                $xml->next('item');

//                opcache_reset();

//                echo $item->title . "\n";
//                usleep(5000);
//                echo $i++ . "\n";
//

            }
            $this->removeNoUpdated($shopUrl);
            echo 'Memory usage at end: ' . memory_get_usage() . "\n";

        } catch (\Exception $e) {
            $this->errors[] = $e->getMessage();
        }

    }

    private function setToUpdate($shopUrl){

        $qb = $this->em->createQueryBuilder();
        $qb->update(Product::class, 'p')
            ->set('p.toUpdate', '1')
            ->where('p.shop = :shopurl')
            ->setParameter('shopurl', $shopUrl)
        ;
        $qb->getQuery()->execute();
    }

    private function removeNoUpdated($shopUrl){

        $qb = $this->em->createQueryBuilder();
        $qb->delete(Product::class, 'p')
            ->where('p.shop = :shopurl')
            ->andWhere('p.toUpdate = 1')
            ->setParameter('shopurl', $shopUrl)
        ;
        $qb->getQuery()->execute();
    }

    public function importImages()
    {
        $productRepository = $this->em->getRepository(Product::class);
        $products = $productRepository->findImagesToImport(500);

        /** @var Product $product */
        foreach ($products as $product){
            echo "Pobieram: " . $product->getImage() . "\n";
            $fileName = pathinfo(parse_url($product->getImage(), PHP_URL_PATH), PATHINFO_FILENAME);
            $fileName = md5(time() . $fileName);

            $fileContent = @file_get_contents($product->getImage());
            if(!$fileContent){
                $product->setImageProcessedStatus(2);
                $this->em->flush();
                echo "Nie udało sie pobrać pliku: " . $product->getImage() . "\n";
                continue;
            }

            file_put_contents($this->param->get("app.import-xml.image-dir") . '/' . $fileName, $fileContent);

            if(!is_file($this->param->get("app.import-xml.image-dir") . '/' . $fileName)){
                echo "Brak pliku: " . $this->param->get("app.import-xml.image-dir") . '/' . $fileName . "\n";
                continue;
            }
            $image = getimagesize($this->param->get("app.import-xml.image-dir") . '/' . $fileName);
            if(!$image){
                echo "Problem z getimagesize: " . $this->param->get("app.import-xml.image-dir") . '/' . $fileName . "\n";
                continue;
            }
            switch ($image['mime']){
                case 'image/png':
                    $fileNewName = md5(time() . $fileName) . '.png';
                    $fileNewNamePath = $fileNewName[0] . '/' . $fileNewName[1];
                    if(!is_dir($this->param->get("app.import-xml.image-dir") . '/' . $fileNewNamePath)){
                        mkdir($this->param->get("app.import-xml.image-dir") . '/' . $fileNewNamePath, 0777, true);
                    }
                    Image::open($this->param->get("app.import-xml.image-dir") . '/' . $fileName)
                        ->cropResize(400, 500, '0xffffff')
                        ->save($this->param->get("app.import-xml.image-dir") . '/' . $fileNewNamePath . '/' .  $fileNewName);
                    unlink($this->param->get("app.import-xml.image-dir") . '/' . $fileName);
//                    rename($this->param->get("app.import-xml.image-dir") . '/' . $fileName, $this->param->get("app.import-xml.image-dir") . '/' . $fileNewNamePath . '/' .  $fileNewName);
                    $product->setImageProcessed($fileNewNamePath . '/' .$fileNewName);
                    $product->setImageProcessedStatus(1);
                    break;
                case 'image/jpeg':
                    $fileNewName = md5(time() . $fileName) . '.jpg';
                    $fileNewNamePath = $fileNewName[0] . '/' . $fileNewName[1];
                    if(!is_dir($this->param->get("app.import-xml.image-dir") . '/' . $fileNewNamePath)){
                        mkdir($this->param->get("app.import-xml.image-dir") . '/' . $fileNewNamePath, 0777, true);
                    }
                    Image::open($this->param->get("app.import-xml.image-dir") . '/' . $fileName)
                        ->cropResize(400, 500, '0xffffff')
                        ->save($this->param->get("app.import-xml.image-dir") . '/' . $fileNewNamePath . '/' .  $fileNewName);
                    unlink($this->param->get("app.import-xml.image-dir") . '/' . $fileName);
//                    rename($this->param->get("app.import-xml.image-dir") . '/' . $fileName, $this->param->get("app.import-xml.image-dir") . '/' . $fileNewNamePath . '/' .  $fileNewName);
                    $product->setImageProcessed($fileNewNamePath . '/' .$fileNewName);
                    $product->setImageProcessedStatus(1);
                    break;
            }
            usleep(1500);
            $this->em->flush();
        }


    }

}