<?php


use App\DTO\ClassifiedListOutputDTO;
use App\Entity\Classified;
use App\Event\StatsClassifiedEvent;
use App\Event\StatsClassifiedViewEvent;
use App\Model\Api\Json\ClassifiedData;
use App\Repository\ClassifiedRepository;
use App\Service\ApiService;
use App\Service\ClassifiedService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ClassifiedController extends AbstractController{

    private $classifiedRepository;
    private $classifiedService;
    private $apiResponseService;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    public function __construct(ClassifiedRepository $classifiedRepository, ClassifiedService $classifiedService, ApiService $apiResponseService, EventDispatcherInterface $dispatcher)
    {
        $this->classifiedRepository = $classifiedRepository;
        $this->classifiedService = $classifiedService;
        $this->apiResponseService = $apiResponseService;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @Route("api/classified", name="api_classified_search", methods={"POST"})
     */
    public function search(Request $request): Response
    {
        $jsonContent = $request->getContent();
        if (empty($jsonContent)) {
            $this->apiResponseService->getErrorResponse('bad_request', 400);
            die();
        }
        /** @var ClassifiedData $params */
        $params = $this->classifiedService->validateFormData($jsonContent, ClassifiedData::class, 'json');

        $outputData = [];
        if(!$params->hideMeta){
            $outputData['meta'] = $this->classifiedService->getClassifiedSearchMeta($params);
        }
        $outputData['data'] = $this->classifiedService->getClassifiedOutputList($this->classifiedService->getClassifiedSearch($params));

        return $this->json($outputData);
    }

    /**
     * @Route("api/classified-xlsx", name="api_classified_xls", methods={"POST"})
     */
    public function classifiedXls(Request $request): Response
    {
        $jsonContent = $request->getContent();
        if (empty($jsonContent)) {
            $this->apiResponseService->getErrorResponse('bad_request', 400);
            die();
        }
        $params = $this->classifiedService->validateFormData($jsonContent, ClassifiedData::class, 'json');

        $this->classifiedService->getClassifiedSearchXls($params);

        return new Response();
    }

    /**
     * @Route("api/classified/map", name="api_classified_search_map", methods={"POST"})
     */
    public function mapSearch(Request $request): Response
    {
        $jsonContent = $request->getContent();
        if (empty($jsonContent)) {
            $this->apiResponseService->getErrorResponse('bad_request', 400);
            die();
        }
        $params = $this->classifiedService->validateFormData($jsonContent, ClassifiedData::class, 'json');

        $outputData = [];
        if($params->showMeta){
            $outputData['meta'] = $this->classifiedService->getClassifiedSearchMeta($params);
        }
        $outputData['data'] = $this->classifiedService->getClassifiedOutputMapList($this->classifiedService->getClassifiedSearchMap($params));

        return $this->json($outputData);
    }

    /**
     * @Route("api/classified/nearby", name="api_classified_search_nearby", methods={"POST"})
     */
    public function nearbySearch(Request $request): Response
    {
        $jsonContent = $request->getContent();
        if (empty($jsonContent)) {
            $this->apiResponseService->getErrorResponse('bad_request', 400);
            die();
        }
        $params = $this->classifiedService->validateFormData($jsonContent, ClassifiedData::class, 'json');

        $outputData = [];
        $outputData['data'] = $this->classifiedService->getClassifiedOutputList($this->classifiedService->getClassifiedSearchNearby($params));

        return $this->json($outputData);
    }

    /**
     * @Route("api/classified/{id}", name="api_classified_info", methods={"GET"})
     */
    public function info(Classified $classified, Request $request): Response
    {
        if(!$classified){
            $this->apiResponseService->getErrorResponse('bad_request', 400);
            die();
        }

        if(!$classified->getIsActive()){
            $this->apiResponseService->getErrorResponse('bad_request', 400);
            die();
        }

        $nowDate = new \DateTime();
        if($nowDate->getTimestamp() > $classified->getDateEnd()->getTimestamp()){
            $this->apiResponseService->getErrorResponse('bad_request', 400);
            die();
        }

        $this->dispatcher->dispatch(new StatsClassifiedViewEvent($classified, $request->server->get("HTTP_X_REAL_IP")?$request->server->get("HTTP_X_REAL_IP"):$request->server->get("REMOTE_ADDR")));


        $outputData = [];
        $outputData['data'] = $this->classifiedService->fixDTOUrl($this->classifiedService->getClassifiedOutputSingle($classified));

        return $this->json($outputData);
    }


}
