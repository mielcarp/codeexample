<?php


namespace Event;


use App\Entity\Classified;

class ClassifiedFilePdfEvent
{
    /**
     * @var $classified Classified
     */
    private $classified;

    public function __construct(Classified $classified)
    {
        $this->classified = $classified;
    }

    /**
     * @return Classified
     */
    public function getClassified(): Classified
    {
        return $this->classified;
    }
}