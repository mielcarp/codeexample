<?php


namespace EventSubscriber;

use App\Entity\Classified;
use App\Event\ClassifiedFilePdfEvent;
use App\Event\ClassifiedFileZipEvent;
use App\Repository\ClassifiedFileRepository;
use App\Service\ClassifiedService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ClassifiedFilePdfSubscriber implements EventSubscriberInterface
{
    private $param;

    private $classifiedService;

    public function __construct(EntityManagerInterface $em, ParameterBagInterface $param, ClassifiedService $classifiedService)
    {
        $this->em = $em;
        $this->param = $param;
        $this->classifiedService = $classifiedService;
    }


    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            ClassifiedFilePdfEvent::class => 'classifiedFilePdf'
        ];
    }

    public function classifiedFilePdf(ClassifiedFilePdfEvent $e){

        $this->classifiedService->makePdf($e->getClassified());
        $this->classifiedService->makePdf($e->getClassified(), 'en');

    }

}