<?php


namespace Listener;


use App\Entity\Classified;
use App\Repository\TerytTercRepository;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ClassifiedPostLoadListener
{
    private $terytTercRepository;
    private $param;

    public function __construct(TerytTercRepository $terytTercRepository, ParameterBagInterface $param)
    {
        $this->terytTercRepository = $terytTercRepository;
        $this->param = $param;
    }

    public function postLoad(Classified $classified) {
        if($classified->getLocalizationVoivodeship()){
            $terytTerc = $this->terytTercRepository->findOneBy(['idVoivodeship' => $classified->getLocalizationVoivodeship()]);
            if($terytTerc){
                $classified->setLocalizationVoivodeshipText(mb_convert_case($terytTerc->getName(),  MB_CASE_LOWER));
            }
        }
        if($classified->getLocalizationDistrict()){
            $district = explode('-', $classified->getLocalizationDistrict());
            if(count($district) > 1){
                $terytTerc = $this->terytTercRepository->findOneBy(['idVoivodeship' => $classified->getLocalizationVoivodeship(), 'idDistrict' => $district[1]]);
                if($terytTerc){
                    $classified->setLocalizationDistrictText($terytTerc->getName());
                }
            }
        }

        $classified->setClassifiedLink($this->param->get('app.site_url') . '/szczegoly_ogloszenia/'.$classified->getId().'/'.($classified->getTypeClassified()==1?'0':'1').'/' . $classified->getTipi());

        $pdfFile = $this->param->get('app.public_dir_path') . '/' . $this->param->get('app.classified_pdf_dir') . '/' . $classified->getId() . '.pdf';
        if(is_file($pdfFile)){
            $classified->setClassifiedLinkPdf($this->param->get('app.site_url_api') . '/get/classified-pdf/' . $classified->getId());
        }

        $now = new \DateTime();
        if($classified->getDiscountPrice() > 0 && $classified->getDiscountStart() != null && $classified->getDiscountEnd() != null && $classified->getDiscountStart()->getTimestamp() < $now->getTimestamp() && $classified->getDiscountEnd()->getTimestamp() > $now->getTimestamp()){
            $classified->setIsDiscount(true);
        }

    }

}