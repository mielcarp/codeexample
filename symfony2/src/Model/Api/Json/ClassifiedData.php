<?php



use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Validator;

class ClassifiedData
{

    /**
     * @Assert\Type(
     *     type="integer",
     *     message="bad_showMeta"
     * )
     * @SerializedName("showMeta")
     */
    public $hideMeta = 0;

    /**
     * @Assert\Type(
     *     type="string",
     *     message="bad_name"
     * )
     * @SerializedName("name")
     */
    public $name = "";

    /**
     * @Assert\Type(
     *     type="string",
     *     message="bad_localizationStreet"
     * )
     * @SerializedName("localizationStreet")
     */
    public $localizationStreet = "";

    /**
     * @Assert\Type(
     *     type="string",
     *     message="bad_localizationTown"
     * )
     * @SerializedName("localizationTown")
     */
    public $localizationTown = "";

    /**
     * @Assert\Type(
     *     type="integer",
     *     message="bad_page"
     * )
     * @SerializedName("currentPage")
     */
    public $currentPage = 1;

    /**
     * @Assert\Type(
     *     type="integer",
     *     message="bad_page"
     * )
     * @SerializedName("itemsPerPage")
     */
    public $itemsPerPage = 12;

    /**
     * @SerializedName("typeClassified")
     * @Assert\Type(
     *     type="integer",
     *     message="bad_typeClassified"
     * )
     */
    public $typeClassified = 0;

    /**
     * @SerializedName("typeProperty")
     * @Assert\Type(
     *     type="integer",
     *     message="bad_typeProperty"
     * )
     */
    public $typeProperty = 0;


    /**
     * @SerializedName("isInPreparation")
     * @Assert\Type(
     *     type="integer",
     *     message="bad_isInPreparation"
     * )
     */
    public $isInPreparation = 0;


    /**
     * @SerializedName("isNew")
     * @Assert\Type(
     *     type="integer",
     *     message="bad_isNew"
     * )
     */
    public $isNew = 0;

    /**
     * @SerializedName("isNewPrice")
     * @Assert\Type(
     *     type="integer",
     *     message="bad_isNewPrice"
     * )
     */
    public $isNewPrice = 0;

    /**
     * @SerializedName("isMakeOffer")
     * @Assert\Type(
     *     type="integer",
     *     message="bad_isMakeOffer"
     * )
     */
    public $isMakeOffer = 0;

    /**
     * @SerializedName("isSold")
     * @Assert\Type(
     *     type="integer",
     *     message="bad_isSold"
     * )
     */
    public $isSold = 0;

    /**
     * @SerializedName("isDiscount")
     * @Assert\Type(
     *     type="integer",
     *     message="bad_isDiscount"
     * )
     */
    public $isDiscount = 0;

    /**
     * @SerializedName("isRecommended")
     * @Assert\Type(
     *     type="integer",
     *     message="bad_isRecommended"
     * )
     */
    public $isRecommended = 0;

    /**
     * @SerializedName("localizationVoivodeship")
     * @Assert\Type(
     *     type="integer",
     *     message="bad_localizationVoivodeship"
     * )
     */
    public $localizationVoivodeship = null;

    /**
     * @SerializedName("localizationDistrict")
     * @Assert\Type(
     *     type="string",
     *     message="bad_localizationDistrict"
     * )
     */
    public $localizationDistrict = '';

    /**
     * @SerializedName("price")
     * @Assert\Collection(
     *     fields = {
     *         "gte" = @Assert\Type(
     *                      type="integer",
     *                      message="bad_price_gte"
     *         ),
     *         "lte" = @Assert\Type(
     *                      type="integer",
     *                      message="bad_price_lte"
     *         )
     *     },
     *     allowMissingFields = true,
     *     extraFieldsMessage="bad_price"
     * )
     */
    public $price = [];

    /**
     * @SerializedName("areaBuilding")
     * @Assert\Collection(
     *     fields = {
     *         "gte" = @Assert\Type(
     *                      type="integer",
     *                      message="bad_areaBuilding_gte"
     *         ),
     *         "lte" = @Assert\Type(
     *                      type="integer",
     *                      message="bad_areaBuilding_lte"
     *         )
     *     },
     *     allowMissingFields = true,
     *     extraFieldsMessage="bad_areaBuilding"
     * )
     */
    public $areaBuilding = [];

    /**
     * @SerializedName("areaGround")
     * @Assert\Collection(
     *     fields = {
     *         "gte" = @Assert\Type(
     *                      type="integer",
     *                      message="bad_areaGround_gte"
     *         ),
     *         "lte" = @Assert\Type(
     *                      type="integer",
     *                      message="bad_areaGround_lte"
     *         )
     *     },
     *     allowMissingFields = true,
     *     extraFieldsMessage="bad_areaGround"
     * )
     */
    public $areaGround = [];

    /**
     * @SerializedName("areaRental")
     * @Assert\Collection(
     *     fields = {
     *         "gte" = @Assert\Type(
     *                      type="integer",
     *                      message="bad_areaRental_gte"
     *         ),
     *         "lte" = @Assert\Type(
     *                      type="integer",
     *                      message="bad_areaRental_lte"
     *         )
     *     },
     *     allowMissingFields = true,
     *     extraFieldsMessage="bad_areaRental"
     * )
     */
    public $areaRental = [];

    /**
     * @SerializedName("areaRentalOffice")
     * @Assert\Collection(
     *     fields = {
     *         "gte" = @Assert\Type(
     *                      type="integer",
     *                      message="bad_areaRentalOffice_gte"
     *         ),
     *         "lte" = @Assert\Type(
     *                      type="integer",
     *                      message="bad_areaRentalOffice_lte"
     *         )
     *     },
     *     allowMissingFields = true,
     *     extraFieldsMessage="bad_areaRentalOffice"
     * )
     */
    public $areaRentalOffice = [];

    /**
     * @SerializedName("areaRentalWarehouse")
     * @Assert\Collection(
     *     fields = {
     *         "gte" = @Assert\Type(
     *                      type="integer",
     *                      message="bad_areaRentalWarehouse_gte"
     *         ),
     *         "lte" = @Assert\Type(
     *                      type="integer",
     *                      message="bad_areaRentalWarehouse_lte"
     *         )
     *     },
     *     allowMissingFields = true,
     *     extraFieldsMessage="bad_areaRentalWarehouse"
     * )
     */
    public $areaRentalWarehouse = [];

    /**
     * @SerializedName("areaRentalRetail")
     * @Assert\Collection(
     *     fields = {
     *         "gte" = @Assert\Type(
     *                      type="integer",
     *                      message="bad_areaRentalRetail_gte"
     *         ),
     *         "lte" = @Assert\Type(
     *                      type="integer",
     *                      message="bad_areaRentalRetail_lte"
     *         )
     *     },
     *     allowMissingFields = true,
     *     extraFieldsMessage="bad_areaRentalRetail"
     * )
     */
    public $areaRentalRetail = [];


    /**
     * @SerializedName("geolocation")
     * @Assert\Collection(
     *     fields = {
     *         "lat" = @Assert\Type(
     *                      type="float",
     *                      message="bad_geolocation_lat"
     *         ),
     *         "lng" = @Assert\Type(
     *                      type="float",
     *                      message="bad_geolocation_lng"
     *         ),
     *         "radius" = @Assert\Type(
     *                      type="integer",
     *                      message="bad_geolocation_radius"
     *         ),
     *     },
     *     allowMissingFields = true,
     *     extraFieldsMessage="bad_geolocation"
     * )
     */
    public $geolocation = [];

    /**
     * @Assert\Type(
     *     type="string",
     *     message="bad_order"
     * )
     * @SerializedName("order")
     * Values: recommended, az, za, price_asc, price_desc, date_asc, date_desc
     */
    public $order = 'recommended';


    /**
     * @SerializedName("isSpecialOffer")
     * @Assert\Type(
     *     type="integer",
     *     message="bad_isSpecialOffer"
     * )
     */
    public $isSpecialOffer = 0;


    /**
     * @SerializedName("specialOfferType")
     * @Assert\Type(
     *     type="integer",
     *     message="bad_specialOfferType"
     * )
     */
    public $specialOfferType = 0;

    /**
     * @SerializedName("isTeletechnicalInfrastructure")
     * @Assert\Type(
     *     type="integer",
     *     message="bad_isTeletechnicalInfrastructure"
     * )
     */
    public $isTeletechnicalInfrastructure = 0;

    /**
     * @SerializedName("isVideoDrone")
     * @Assert\Type(
     *     type="integer",
     *     message="bad_isVideoDrone"
     * )
     */
    public $isVideoDrone = 0;

    /**
     * @SerializedName("isConcept")
     * @Assert\Type(
     *     type="integer",
     *     message="bad_isConcept"
     * )
     */
    public $isConcept = 0;

    /**
     * @SerializedName("mpzp")
     * @Assert\Type(
     *     type="integer",
     *     message="bad_mpzp"
     * )
     */
    public $mpzp = 0;

    /**
     * @SerializedName("classifiedId")
     * @Assert\Type(
     *     type="integer",
     *     message="bad_classifiedId"
     * )
     */
    public $classifiedId = 0;

}