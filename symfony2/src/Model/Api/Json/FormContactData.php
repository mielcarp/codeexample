<?php


namespace App\Model\Api\Json;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\SerializedName;
use App\Validator as AppAssert;

class FormContactData
{
    /**
     * @Assert\Email(
     *     message="bad_email"
     * )
     * @SerializedName("email")
     */
    public $email = "";

    /**
     * @Assert\Type(
     *     type="string",
     *     message="bad_subject"
     * )
     * @Assert\Regex(
     *     pattern="/^[0-9\!\@\#\(\)\%\t\n\sAaĄąBbCcĆćDdEeĘęFfGgHhIiJjKkLlŁłMmNnŃńOoÓóPpRrSsŚśTtUuWwVvYyZzŹźŻż\.\,\?\-\_\+\=]+$/i",
     *     message="bad_subject"
     * )
     * @SerializedName("subject")
     */
    public $subject = "";

    /**
     * @Assert\Type(
     *     type="string",
     *     message="bad_message"
     * )
     * @Assert\Regex(
     *     pattern="/^[0-9\!\@\#\(\)\%\t\n\sAaĄąBbCcĆćDdEeĘęFfGgHhIiJjKkLlŁłMmNnŃńOoÓóPpRrSsŚśTtUuWwVvYyZzŹźŻż\.\,\?\-\_\+\=]+$/i",
     *     message="bad_message"
     * )
     * @SerializedName("message")
     */
    public $message = "";

    /**
     * @Assert\Type(
     *     type="boolean",
     *     message="bad_agremment1"
     * )
     * @Assert\IsTrue(message="bad_agremment1")
     * @SerializedName("agremment1")
     */
    public $agremment1;

    /**
     * @Assert\NotBlank(
     *     message="bad_token"
     * )
     * @Assert\Type(
     *     type="string",
     *     message="bad_token"
     * )
     * @AppAssert\ReCaptcha(
     *     disable=false
     * )
     * @SerializedName("token")
     */
    public $token = "";

    /**
     * @Assert\Type(
     *     type="string",
     *     message="bad_eventID"
     * )
     * @SerializedName("eventID")
     */
    public $eventID = "";

}