<?php


namespace Service;


use App\Entity\Classified;
use App\Entity\ClassifiedExport;
use App\Repository\ClassifiedExportRepository;
use App\Repository\ClassifiedRepository;
use Doctrine\Migrations\Configuration\Exception\FileNotFound;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use function App\Service\dump;

class ClassifiedExportService
{
    private $em;
    private $classifiedExportRepository;
    private $templating;
    private $param;

    private $exportImages = [];
    private $tmpDirName;
    /**
     * @var ClassifiedRepository
     */
    private $classifiedRepository;

    public function __construct(EntityManagerInterface $em, ClassifiedExportRepository $classifiedExportRepository,
                                ParameterBagInterface $param,
                                \Twig\Environment $templating, ClassifiedRepository $classifiedRepository)
    {
        $this->em = $em;
        $this->classifiedExportRepository = $classifiedExportRepository;
        $this->templating = $templating;
        $this->param = $param;
        $this->tmpDirName = uniqid();
        $this->classifiedRepository = $classifiedRepository;
    }

    public function exportEvent(Classified $classified)
    {
        if ($classified->getIsExported()) {
            $status = 1; //do eksportu
        } else {
            //$status = 2; //do ususniecia
            return;
        }

        if ($classifiedExport = $this->classifiedExportRepository->findOneBy(['classified' => $classified->getId()])) {
            $classifiedExport->setStatus($status);
            $classifiedExport->setCreatedAt(new \DateTime());
            $this->em->flush();
        } else {
            $classifiedExport = new ClassifiedExport();
            $classifiedExport->setClassified($classified);
            $classifiedExport->setCreatedAt(new \DateTime());
            $classifiedExport->setStatus($status);
            $this->em->persist($classifiedExport);
            $this->em->flush();
        }
    }

    public function changeForExport($classifieds)
    {
        $ret = [];
        if (empty($classifieds)) {
            return $ret;
        }


        /**
         * @var $classifiedExport ClassifiedExport
         */
        foreach ($classifieds as $classifiedExport) {
            if (get_class($classifiedExport) == 'App\Entity\ClassifiedExport') {
                switch ($classifiedExport->getClassified()->getTypeProperty()) {
                    case 3:
                        $type = 'dzialki';
                        break;
                    case 5:
                        $type = 'mieszkania';
                        break;
                    default:
                        $type = 'lokale';
                }

                if (!isset($ret[$type])) {
                    $ret[$type] = [];
                }

                $ret[$type][] = $classifiedExport->getClassified();
            } else {
                $classified = $classifiedExport;
                switch ($classifiedExport->getTypeProperty()) {
                    case 3:
                        $type = 'dzialki';
                        break;
                    case 5:
                        $type = 'mieszkania';
                        break;
                    default:
                        $type = 'lokale';
                }

                if (!isset($ret[$type])) {
                    $ret[$type] = [];
                }

                $ret[$type][] = $classified;
            }
        }

        return $ret;
    }

    public function makeZip($classifieds)
    {
        $classifiedsForExport = $this->changeForExport($classifieds);

        $exportXmlContent = $this->templating->render('xml/classifiedForExport.xml.twig',
            [
                'dateTime' => new \DateTime(),
                'appUrl' => $this->param->get("app.site_url_api"),
                'classifiedsForExport' => $classifiedsForExport
            ]
        );

        $exportTmpPath = $this->param->get("app.classified_export_tmp") . '/' . $this->tmpDirName . '/';
        if (!is_dir($exportTmpPath)) {
            mkdir($exportTmpPath, 0777);
        }

        $exportXmlFilePath = $exportTmpPath . 'oferty.xml';

        $archiveFilePath = $exportTmpPath . 'oferty_' . date('YmdHis') . '.zip';
        file_put_contents($exportXmlFilePath, $exportXmlContent);

        $zip = new \ZipArchive();
        if ($zip->open($archiveFilePath, \ZipArchive::CREATE) === true) {
            $zip->addFile($exportXmlFilePath, basename($exportXmlFilePath));
            foreach ($classifiedsForExport as $classifieds) {
                foreach ($classifieds as $classified) {
                    $photos = $this->makeImage($classified);
                    if (!empty($photos)) {
                        foreach ($photos as $photo) {
                            $zip->addFile($photo['path'], $photo['name']);
                        }
                    }
                }
            }
            $zip->close();
        }

        if (!is_file($archiveFilePath)) {
            throw new FileNotFound($archiveFilePath);
        }

        return $archiveFilePath;
    }

    public function unlinkTmpDir()
    {
        echo "kasuje katalog tymczasowy\n";
        array_map('unlink', array_filter((array)glob($this->param->get("app.classified_export_tmp") . '/' . $this->tmpDirName . '/*')));
        rmdir($this->param->get("app.classified_export_tmp") . '/' . $this->tmpDirName);
    }

    public function makeImage(Classified $classified)
    {

        if($classified->getIsSold() == 1 || $classified->getIsActive() == 0 || $classified->getIsMakeOffer() == 1){
            return;
        }
        /**
         * @var $classified Classified
         */
        $waterMarkPath = $this->param->get("app.classified_export_img") . '/watermark.png';
        $wmBackgroundPath = $this->param->get("app.classified_export_img") . '/watermark-bg.png';
        $discountStampPath = $this->param->get("app.classified_export_img") . '/promocja.png';

        $exportImages = [];

        if ($classified->getClassifiedPhotos()->count()) {
            foreach ($classified->getClassifiedPhotos() as $classifiedPhoto) {
                $exportTmpPath = $this->param->get("app.classified_export_tmp") . '/' . $this->tmpDirName . '/';
                $photoName = basename($classifiedPhoto->getFilePath());
                $photoPath = $this->param->get("app.public_dir_path") . '/' . $classifiedPhoto->getFilePath();
                if(!is_file($photoPath)){
                    echo 'Brak pliku: ' . $photoPath . "\n";
                    echo "Pomijam\n";
                    continue;
                }
                $tmpPhotoPath = $exportTmpPath . $photoName;
                $tmpBgPath = $exportTmpPath . 'bg_' . pathinfo($photoName, PATHINFO_FILENAME) . '.png';

                // Load the stamp and the photo to apply the watermark to
                $stamp = imagecreatefrompng($waterMarkPath);
                $stampBackground = imagecreatefrompng($wmBackgroundPath);
                $ext = pathinfo($photoPath, PATHINFO_EXTENSION);
                $isPng = ($ext == 'png');

                if ($isPng) {
                    $im = imagecreatefrompng($photoPath);
                } else {
                    $im = imagecreatefromjpeg($photoPath);
                }

                // Set the margins for the stamp and get the height/width of the stamp image
                $marge_right = 20;
                $marge_bottom = 15;
                $sx = imagesx($stamp);
                $sy = imagesy($stamp);

                $ix = imagesx($im);
                $iy = imagesy($im);

                //resize stamp background to fit base image width
                $bgx = imagesx($stampBackground);
                $bgy = imagesy($stampBackground);
                $newHeight = $bgy;
                $newWidth = $ix;

                $stampBgResized = imagecreatetruecolor($newWidth, $newHeight);
                imagealphablending($stampBgResized, false);
                imagesavealpha($stampBgResized, true);
                $transparent = imagecolorallocatealpha($stampBgResized, 255, 255, 255, 127);
                imagefilledrectangle($stampBgResized, 0, 0, $newWidth, $newHeight, $transparent);
                imagecopyresampled($stampBgResized, $stampBackground, 0, 0, 0, 0, $newWidth, $newHeight, $bgx, $bgy);
                imagepng($stampBgResized, $tmpBgPath);
                imagedestroy($stampBgResized);
                $stampBgResized = imagecreatefrompng($tmpBgPath);

                //add stamp onto resized background using the margin offsets
                imagecopy($stampBgResized, $stamp, $newWidth - $sx - $marge_right, $newHeight - $sy, 0, 0, $sx, $sy);


                // Copy the stamp image onto our photo using photo
                // width to calculate positioning of the stamp.
                imagecopy($im, $stampBgResized, $ix - $newWidth, $iy - $newHeight, 0, 0, $newWidth, $newWidth);

                if ($classified->getDiscountValue() > 0 && $classified->getDiscountStart() <= (new \DateTime()) && $classified->getDiscountEnd() > (new \DateTime())) {
                    $discountStamp = imagecreatefrompng($discountStampPath);
                    $dsWidth = imagesx($discountStamp);
                    $dsHeight = imagesy($discountStamp);

                    imagesavealpha($discountStamp, true);
                    imagealphablending($discountStamp, true);
                    imagecopy($im, $discountStamp, 0, 0, 0, 0, $dsWidth, $dsHeight);
                }

                // Output and free memory
                if ($isPng) {
                    imagepng($im, $tmpPhotoPath);
                } else {
                    imagejpeg($im, $tmpPhotoPath);
                }
                imagedestroy($im);
                $exportImages[] = [
                    'path' => $tmpPhotoPath,
                    'name' => basename($tmpPhotoPath)
                ];
                unlink($tmpBgPath);
            }
        }

        return $exportImages;
    }

    public function removeFromQueue($classifieds)
    {
        if (!empty($classifieds)) {
            foreach ($classifieds as $classified) {
                $this->em->remove($classified);
            }
            $this->em->flush();
        }
    }

    public function runExport()
    {
        $classifieds = $this->classifiedExportRepository->findWaiting();
//        $this->tmpDirName = 'dev_test';

        if (empty($classifieds)) {
            echo "Brak ogłoszeń do eksportu\n";
            die();
        }

        echo "tworze plik zip\n";
        $fileName = $this->makeZip($classifieds);
        echo "plik zip stworzony\n";

        if (is_file($fileName)) {
            try {
                $ftp = new \FtpClient\FtpClient();
                echo "Laczenie z ftp\n";
//                echo "Host: " . $this->param->get("app.export.host") . "\n";
//                echo "Login: " . $this->param->get("app.export.user"). "\n";
//                echo "Pass: " . $this->param->get("app.export.pass"). "\n";
                $ftp->connect($this->param->get("app.export.host"));
                $ftp->login($this->param->get("app.export.user"), $this->param->get("app.export.pass"));
                $ftp->pasv(true);
                $ftp->putFromPath($fileName);
                $ftp->close();
                echo "Plik wgrany na ftp\n";
                echo "Kasuje ogłoszenia wyeksportowane z kolejki\n";
                $this->removeFromQueue($classifieds);
            } catch (\FtpException $e) {
                dump($e);
            }
        }
        $this->unlinkTmpDir();
    }

    public function runExportAll($noupload = 0)
    {
        $classifieds = $this->classifiedRepository->findBy(['isExported' => 1]);

//        $this->tmpDirName = 'dev_test';

        if (empty($classifieds)) {
            echo "Brak ogłoszeń do eksportu\n";
            die();
        }

        echo "tworze plik zip\n";
        $fileName = $this->makeZip($classifieds);
        echo "plik zip stworzony\n";

        if($noupload){
            echo "Koniec bez wgrywania pliku: $fileName\n";
            return;
        }

        if (is_file($fileName)) {
            try {
                $ftp = new \FtpClient\FtpClient();
                echo "Laczenie z ftp\n";
                echo "Host: " . $this->param->get("app.export.host") . "\n";
                echo "Login: " . $this->param->get("app.export.user"). "\n";
                $ftp->connect($this->param->get("app.export.host"));
                $ftp->login($this->param->get("app.export.user"), $this->param->get("app.export.pass"));
                $ftp->pasv(true);
                $ftp->putFromPath($fileName);
                $ftp->close();
                echo "Plik wgrany na ftp\n";
//                echo "Kasuje ogłoszenia wyeksportowane z kolejki\n";
//                $this->removeFromQueue($classifieds);
            } catch (\FtpException $e) {
                dump($e);
            }
        }
        $this->unlinkTmpDir();
    }


    public function getExport(Classified $classified)
    {
        $classifieds[] = $classified;

        if (empty($classifieds)) {
            echo "Brak ogłoszeń do eksportu\n";
            die();
        }

        return $this->makeZip($classifieds);

    }


}