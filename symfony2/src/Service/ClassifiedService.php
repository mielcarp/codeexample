<?php

namespace Service;

use App\Entity\ClassifiedFile;
use App\Entity\Label;
use App\Entity\StatsClassified;
use App\Repository\ClassifiedFileRepository;
use App\Repository\LabelRepository;
use App\Repository\StatsClassifiedRepository;
use App\Service\ApiService;
use App\Service\Symfony;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Html;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Spipu\Html2Pdf\Html2Pdf;
use App\DTO\ClassifiedListOutputDTO;
use App\DTO\ClassifiedMapListOutputDTO;
use App\DTO\ClassifiedSingleOutputDTO;
use App\Entity\Classified;
use App\Model\Api\Json\ClassifiedData;
use App\Repository\ClassifiedRepository;
use App\Service\TerytTercService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ClassifiedService
{

    protected $em;
    protected $classifiedRepository;
    protected $apiService;
    protected $serializer;
    protected $validator;
    protected $terytTercService;
    protected $slugger;
    protected $param;
    protected $templating;
    protected $classifiedFileRepository;
    protected $statsClassifiedRepository;
    private $labelRepository;

    /**
     * ClassifiedService constructor.
     * @param $em
     * @param $classifiedRepository
     */
    public function __construct(
        ApiService                $apiService,
        EntityManagerInterface    $em,
        ClassifiedRepository      $classifiedRepository,
        ClassifiedFileRepository  $classifiedFileRepository,
        SerializerInterface       $serializer,
        ValidatorInterface        $validator,
        TerytTercService          $terytTercService,
        SluggerInterface          $slugger,
        ParameterBagInterface     $param,
        \Twig\Environment         $templating,
        StatsClassifiedRepository $statsClassifiedRepository,
        LabelRepository           $labelRepository
    )
    {
        $this->apiService = $apiService;
        $this->em = $em;
        $this->classifiedRepository = $classifiedRepository;
        $this->serializer = $serializer;
        $this->validator = $validator;
        $this->terytTercService = $terytTercService;
        $this->slugger = $slugger;
        $this->param = $param;
        $this->templating = $templating;
        $this->classifiedFileRepository = $classifiedFileRepository;
        $this->statsClassifiedRepository = $statsClassifiedRepository;
        $this->labelRepository = $labelRepository;
    }


    public function manageNewStatusClassified()
    {

        echo "\n";
        $removeNewStatus = $this->classifiedRepository->removeNewStatus();
        $addNewStatus = $this->classifiedRepository->addNewStatus();

        echo 'Usunięto status nowy: ' . $removeNewStatus . "\n";
        echo 'Dodano status nowy: ' . $addNewStatus . "\n";

        echo "\n";
    }

    public function manageOutdatedClassified()
    {

        echo "\n";
        $removeOutdatedActiveStatus = $this->classifiedRepository->removeOutdatedActiveStatus();

        echo 'Usunięto status aktywny: ' . $removeOutdatedActiveStatus . "\n";

        echo "\n";
    }

    public function getClassifiedSearchMeta(ClassifiedData $params)
    {

        $output['totalItems'] = $this->classifiedRepository->findClassifiedTotal($params);
        $output['totalIsNewItems'] = $this->classifiedRepository->findClassifiedisNewTotal($params);
        $output['totalIsDiscountItems'] = $this->classifiedRepository->findClassifiedDiscountTotal($params);
        $output['totalIsMakeOfferItems'] = $this->classifiedRepository->findClassifiedIsMakeOfferTotal($params);
        $output['totalIsSoldItems'] = $this->classifiedRepository->findClassifiedIsSoldTotal($params);
        $output['totalIsNewPriceItems'] = $this->classifiedRepository->findClassifiedIsNewPriceTotal($params);
        $output['totalIsNegotiablePriceItems'] = $this->classifiedRepository->findClassifiedIsNegotiablePriceTotal($params);
        $output['itemsPerPage'] = $params->itemsPerPage;
        $output['currentPage'] = $params->currentPage;
        $output['voivodeship'] = $this->terytTercService->getVoivodeshipOutput();
        $output['district'] = $this->terytTercService->getDistrictOutput($params);
        $output['typeProperty'] = Classified::$typePropertyArray;

        return $output;
    }

    public function getClassifiedSearch(ClassifiedData $params)
    {
        $output = $this->classifiedRepository->findClassified($params);
        return $output;
    }

    public function getClassifiedSearchMap(ClassifiedData $params)
    {
        $output = $this->classifiedRepository->findClassifiedForMap($params);
        return $output;
    }

    public function getClassifiedSearchNearby(ClassifiedData $params)
    {
        $output = $this->classifiedRepository->findClassifiedNearby($params);
        return $output;
    }


    public function validateFormData($payload, $model, $format)
    {

        $payload = $this->serializer->deserialize($payload, $model, $format, ['disable_type_enforcement' => true]);

        $errors = $this->validator->validate($payload);
        if (count($errors)) {
            $errorList = [];
            /* @var $error Symfony\Component\Validator\ConstraintViolation */
            foreach ($errors as $error) {
                $errorList[] = $error->getMessage();
            }
            $errorList = array_unique($errorList);
            array_multisort($errorList, SORT_DESC);
            $this->apiService->getErrorResponse($errorList, 400);
        }

        return $payload;
    }

//    public function getClassifiedOutput(Classified $classified)
//    {
//        $classifiedOutput = new ClassifiedListOutputDTO($classified);
//
//        return $classifiedOutput;
//    }

    public function getClassifiedOutputSingle(Classified $classified)
    {

        $classifiedSingleOutputDTO = new ClassifiedSingleOutputDTO($classified);

        if ($classified->getRelated() != '') {
            $classifiedSingleOutputDTO->setRelated($this->getRelatedClassified($classified));
        } else {
            $classifiedSingleOutputDTO->setRelated($this->getRandomRelatedClassified($classified));
        }

        return $classifiedSingleOutputDTO;
    }

    public function getRelatedClassified(Classified $classified, $limit = 4)
    {
        $relatedArrayOut = [];
        $relatedIDsArray = explode(",", $classified->getRelated());
        if (empty($relatedIDsArray)) {
            return $this->getRandomRelatedClassified($classified);
        }
        foreach ($relatedIDsArray as $k => $v) {
            if (intval($v) == 0) {
                unset($relatedIDsArray[$k]);
            }
        }

        if (!empty($relatedIDsArray)) {
            $relatedClassifieds = $this->classifiedRepository->findRelatedByIds($relatedIDsArray, $limit);
            foreach ($relatedIDsArray as $relatedIDArray) {
                foreach ($relatedClassifieds as $relatedClassified) {
                    /** @var $relatedClassified Classified */
                    if ($relatedIDArray == $relatedClassified->getId()) {
                        $relatedArrayOut[$relatedIDArray] = new ClassifiedListOutputDTO($relatedClassified);
                    }
                }
            }

            $relatedArrayOut = array_values($relatedArrayOut);

            if (count($relatedArrayOut) < $limit) {
                $relatedArrayOut = array_merge($relatedArrayOut, $this->getRandomRelatedClassified($classified, $limit - count($relatedArrayOut), $relatedIDsArray));
            }
        }

        return $relatedArrayOut;
    }

    public function getRandomRelatedClassified(Classified $classified, $limit = 4, $notIn = [])
    {
        $relatedArrayOut = [];
        $relatedClassifieds = $this->classifiedRepository->findRelatedByVoivodeship($classified, $limit, $notIn);

        if (!empty($relatedClassifieds)) {
            foreach ($relatedClassifieds as $relatedClassified) {
                /** @var $relatedClassified Classified */
                $relatedArrayOut[] = new ClassifiedListOutputDTO($relatedClassified);
            }
        }

        return $relatedArrayOut;
    }

    public function getClassifiedOutputList(array $classifiedList)
    {
        $output = [];

        if (empty($classifiedList)) {
            return [];
        }

        foreach ($classifiedList as $classified) {
            $output[] = new ClassifiedListOutputDTO($classified);
        }

        return $output;
    }

    public function getClassifiedOutputMapList(array $classifiedList)
    {
        $output = [];

        if (empty($classifiedList)) {
            return [];
        }

        foreach ($classifiedList as $classified) {
            $output[] = new ClassifiedMapListOutputDTO($classified);
        }

        return $output;
    }

    public function getClassifiedPdf(Classified $classified, $lang = 'pl')
    {
        $dateToday = new \DateTime();

        if ($classified->getIsActive() == false || $classified->getDateStart() >= $dateToday || $classified->getDateEnd() < $dateToday) {
            throw new NotFoundHttpException('Brak ogłoszenia');
        }

        $pdfFileDir = $this->param->get("app.public_dir_path") . '/' . $this->param->get("app.classified_pdf_dir");

        if ($lang == 'en') {
            $pdfFile = $pdfFileDir . '/' . $classified->getId() . '_en.pdf';
        } else {
            $pdfFile = $pdfFileDir . '/' . $classified->getId() . '.pdf';
        }

        if (!is_file($pdfFile)) {
            throw new NotFoundHttpException('Plik nie istnieje');
        }

        $response = new BinaryFileResponse($pdfFile);
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $this->slugger->slug($classified->getName()) . '.pdf'
        );
        return $response;

    }

    public function makePdf(Classified $classified, $lang = 'pl')
    {
        if (!$classified->getIsActive()) {
            return;
        }

        $pdfDir = $this->param->get('app.public_dir_path') . '/' . $this->param->get('app.classified_pdf_dir');
        if ($lang == 'en') {
            $pdfFile = $pdfDir . '/' . $classified->getId() . '_en.pdf';
        } else {
            $pdfFile = $pdfDir . '/' . $classified->getId() . '.pdf';
        }

        if (!is_dir($pdfDir)) {
            mkdir($pdfDir);
        }
        if (is_file($pdfFile)) {
            unlink($pdfFile);
        }

        $photos = [];
        foreach ($classified->getClassifiedPhotos() as $classifiedPhoto) {
            if (!is_file($this->param->get("app.public_dir_path") . '/' . $classifiedPhoto->getFilePath())) {
                continue;
            }
            $photos[] = $classifiedPhoto;
        }

        switch ($classified->getTypeClassified()) {
            default:
            case 1:
                $pdfHtml = $this->templating->render('pdf/classified-sell' . (($lang == 'en') ? '_en' : '') . '.html.twig',
                    [
                        'appUrl' => $this->param->get("app.site_url_api"),
                        'today' => new \DateTime(),
                        'classified' => $classified,
                        'classifiedPhotoFirst' => (isset($photos[0]) ? $photos[0] : false),
                        'classifiedPhotoSecound' => (isset($photos[1]) ? $photos[1] : false),
                        'advisor' => ($classified->getAdvisor()->first() ? $classified->getAdvisor()->first() : false)
                    ]
                );
                break;
            case 2:
                $pdfHtml = $this->templating->render('pdf/classified-rent' . (($lang == 'en') ? '_en' : '') . '.html.twig',
                    [
                        'appUrl' => $this->param->get("app.site_url_api"),
                        'today' => new \DateTime(),
                        'classified' => $classified,
                        'classifiedPhotoFirst' => (isset($photos[0]) ? $photos[0] : false),
                        'classifiedPhotoSecound' => (isset($photos[1]) ? $photos[1] : false),
                        'advisor' => ($classified->getAdvisor()->first() ? $classified->getAdvisor()->first() : false)
                    ]
                );
                break;
        }

        $html2pdf = new Html2Pdf('P', 'A4', 'pl', true, 'utf-8', array(0, 0, 0, 5));
        $html2pdf->pdf->SetDisplayMode('fullpage');
//        $html2pdf->pdf->AddFont('arial','', $this->param->get("kernel.project_dir") . '/pdf_fonts/arial.php');
//        $html2pdf->pdf->AddFont('arb','', $this->param->get("kernel.project_dir") . '/pdf_fonts/arb.php');
//        $html2pdf->pdf->AddFont('dr','', $this->param->get("kernel.project_dir") . '/pdf_fonts/dr.php');
        $html2pdf->writeHTML($pdfHtml);
        $html2pdf->output($pdfFile, 'F');
        unset($html2pdf);
        unset($classified);
        unset($pdfHtml);

    }

    public function getClassifiedSearchXls(ClassifiedData $params)
    {

        $params->itemsPerPage = 999999;
        $classifiedSearches = $this->getClassifiedSearch($params);

        if ($params->typeClassified == 2) {
            $xlsColumns = [
                'Nazwa',
                'Ulica',
                'Miejscowość',
                'Województwo',
                'Cena wynajmu m2',
                'Pow. Wynajmu',
                'Link',
            ];
        } else {
            $xlsColumns = [
                'Nazwa',
                'Ulica',
                'Miejscowość',
                'Województwo',
                'Cena',
                'Pow. Budynku',
                'Pow. Gruntu',
                'Link',
            ];
        }

        /**
         * @var $classified Classified
         */
        foreach ($classifiedSearches as $k => $classified) {
            if ($params->typeClassified == 2) {
                $xlsData[$k] = [
                    $classified->getName(),//'Ulica',
                    $classified->getLocalizationStreet(),//'Ulica',
                    $classified->getLocalizationTown(),//'Miejscowość',
                    $classified->getLocalizationVoivodeshipText(),//'Województwo',
                    $classified->getPriceSqMeter(), //'Cena',
                    $classified->getAreaRental(),//'Pow. Wynajmu',
                    $classified->getClassifiedLink(),//'Link',
                ];
            } else {
                $price = $classified->getPrice();
                if ($classified->getIsDiscount()) {
                    $price = $classified->getDiscountPrice();
                }
                if ($classified->getIsSold()) {
                    $price = 0;
                }
                $xlsData[$k] = [
                    $classified->getName(),//'Ulica',
                    $classified->getLocalizationStreet(),//'Ulica',
                    $classified->getLocalizationTown(),//'Miejscowość',
                    $classified->getLocalizationVoivodeshipText(),//'Województwo',
                    ($classified->getIsMakeOffer() ? 0 : $price),
                    $classified->getAreaBuilding(),//'Pow. Budynku',
                    $classified->getAreaGround(),//'Pow. Gruntu',
                    $classified->getClassifiedLink(),//'Link',
                ];
            }
        }

        $fileName = 'nieruchomosci_wynik_wyszukiwania_' . date("Y-m-d-H-i-s") . '';

        $this->saveXls($fileName, $xlsColumns, $xlsData, "Wyniki wyszukiwania", false);

    }

    public function getClassifiedRaport($formData)
    {

        if (!$formData['voivodeship']->isEmpty()) {
            $classifieds = $this->classifiedRepository->findByVoivodeship($formData['voivodeship']);
        } else {
            $classifieds = $this->classifiedRepository->findAll();
        }

        //xls columns
        $xlsColumns = [];

        if (isset($formData['ids']) && !empty($formData['ids']) && in_array("id", $formData['ids'])) {
            $xlsColumns[] = 'id';
        }
        if (isset($formData['typeProperty']) && $formData['typeProperty'] == true) {
            $xlsColumns[] = 'Typ oferty';
        }
        if (isset($formData['ids']) && !empty($formData['ids']) && in_array("tipi", $formData['ids'])) {
            $xlsColumns[] = 'TiPi';
        }
        if (isset($formData['ids']) && !empty($formData['ids']) && in_array("tipiAdditional", $formData['ids'])) {
            $xlsColumns[] = 'TIPI dodatkowe';
        }
        if (isset($formData['address']) && !empty($formData['address']) && in_array("name", $formData['address'])) {
            $xlsColumns[] = 'Nazwa';
        }
        if (isset($formData['address']) && !empty($formData['address']) && in_array("localizationStreet", $formData['address'])) {
            $xlsColumns[] = 'Ulica';
        }
        if (isset($formData['address']) && !empty($formData['address']) && in_array("localizationTown", $formData['address'])) {
            $xlsColumns[] = 'Miejscowość';
        }
        if (isset($formData['address']) && !empty($formData['address']) && in_array("localizationVoivodeship", $formData['address'])) {
            $xlsColumns[] = 'Województwo';
        }
        if (isset($formData['address']) && !empty($formData['address']) && in_array("localizationDistrict", $formData['address'])) {
            $xlsColumns[] = 'Powiat';
        }
        if (isset($formData['address']) && !empty($formData['address']) && in_array("localizationLong", $formData['address'])) {
            $xlsColumns[] = 'Długość geograficzna';
        }
        if (isset($formData['address']) && !empty($formData['address']) && in_array("localizationLong", $formData['address'])) {
            $xlsColumns[] = 'Szerokośc geograficzna';
        }
        if (isset($formData['price']) && !empty($formData['price']) && in_array("price", $formData['price'])) {
            $xlsColumns[] = 'Cena';
        }
        if (isset($formData['price']) && !empty($formData['price']) && in_array("priceSqMeter", $formData['price'])) {
            $xlsColumns[] = 'Cena m2';
        }


        if (isset($formData['reservation']) && !empty($formData['reservation']) && in_array("reservationText", $formData['reservation'])) {
            $xlsColumns[] = 'Rezerwacja tekst';
        }
        if (isset($formData['reservation']) && !empty($formData['reservation']) && in_array("reservationTextEN", $formData['reservation'])) {
            $xlsColumns[] = 'Rezerwacja tekst [EN]';
        }
        if (isset($formData['reservation']) && !empty($formData['reservation']) && in_array("reservationDateEnd", $formData['reservation'])) {
            $xlsColumns[] = 'Rezerwacja do';
        }
        if (isset($formData['area']) && !empty($formData['area']) && in_array("areaBuilding", $formData['area'])) {
            $xlsColumns[] = 'Pow. Budynku';
        }
        if (isset($formData['area']) && !empty($formData['area']) && in_array("areaGround", $formData['area'])) {
            $xlsColumns[] = 'Pow. Gruntu';
        }
        if (isset($formData['area']) && !empty($formData['area']) && in_array("areaRental", $formData['area'])) {
            $xlsColumns[] = 'Pow. Wynajmu';
        }
        if (isset($formData['area']) && !empty($formData['area']) && in_array("areaRentalOffice", $formData['area'])) {
            $xlsColumns[] = 'Pow. wynajmu - biurowa';
        }
        if (isset($formData['area']) && !empty($formData['area']) && in_array("areaRentalRetail", $formData['area'])) {
            $xlsColumns[] = 'Pow. wynajmu - handlowa';
        }
        if (isset($formData['area']) && !empty($formData['area']) && in_array("areaRentalWarehouse", $formData['area'])) {
            $xlsColumns[] = 'Pow. wynajmu - magazynowa';
        }
        if (isset($formData['area']) && !empty($formData['area']) && in_array("areaInBuildings", $formData['area'])) {
            $xlsColumns[] = 'Powierzchnia w budynkach';
        }
        if (isset($formData['area']) && !empty($formData['area']) && in_array("areaInGround", $formData['area'])) {
            $xlsColumns[] = 'Powierzchnia w gruncie';
        }
        if (isset($formData['area']) && !empty($formData['area']) && in_array("areaOnGround", $formData['area'])) {
            $xlsColumns[] = 'Powierzchnia na gruncie';
        }
        if (isset($formData['area']) && !empty($formData['area']) && in_array("areaOnRoof", $formData['area'])) {
            $xlsColumns[] = 'Powierzchnia na dachu';
        }
        if (isset($formData['area']) && !empty($formData['area']) && in_array("areaOnFacade", $formData['area'])) {
            $xlsColumns[] = 'Powierzchnia na elewacji';
        }
        if (isset($formData['area']) && !empty($formData['area']) && in_array("areaInBuildingsPP", $formData['area'])) {
            $xlsColumns[] = 'Powierzchnia w budynkach Poczta Polska';
        }
        if (isset($formData['other']) && !empty($formData['other']) && in_array("lawTitle", $formData['other'])) {
            $xlsColumns[] = 'Tytuł prawny';
        }
        if (isset($formData['other']) && !empty($formData['other']) && in_array("conservatorProtection", $formData['other'])) {
            $xlsColumns[] = 'Ochrona konserwatora';
        }
        if (isset($formData['other']) && !empty($formData['other']) && in_array("mpzp", $formData['other'])) {
            $xlsColumns[] = 'MPZP';
        }
        if (isset($formData['publication']) && !empty($formData['publication']) && in_array("dateStart", $formData['publication'])) {
            $xlsColumns[] = 'Data początku publikacji';
        }
        if (isset($formData['publication']) && !empty($formData['publication']) && in_array("dateEnd", $formData['publication'])) {
            $xlsColumns[] = 'Data zakończenia publikacji';
        }
        if (isset($formData['publication']) && !empty($formData['publication']) && in_array("planDate", $formData['publication'])) {
            $xlsColumns[] = 'Planowana data przedstawienia oferty';
        }
        if (isset($formData['desc']) && !empty($formData['desc']) && in_array("descShort", $formData['desc'])) {
            $xlsColumns[] = 'Opis kótki';
        }
        if (isset($formData['desc']) && !empty($formData['desc']) && in_array("descShortEN", $formData['desc'])) {
            $xlsColumns[] = 'Opis kótki [EN]';
        }
        if (isset($formData['desc']) && !empty($formData['desc']) && in_array("desc", $formData['desc'])) {
            $xlsColumns[] = 'Opis ogólny';
        }
        if (isset($formData['desc']) && !empty($formData['desc']) && in_array("descEN", $formData['desc'])) {
            $xlsColumns[] = 'Opis ogólny [EN]';
        }
        if (isset($formData['desc']) && !empty($formData['desc']) && in_array("descLocalization", $formData['desc'])) {
            $xlsColumns[] = 'Opis lokalizacji';
        }
        if (isset($formData['desc']) && !empty($formData['desc']) && in_array("descLocalizationEN", $formData['desc'])) {
            $xlsColumns[] = 'Opis lokalizacji [EN]';
        }
        if (isset($formData['propertyInformation']) && !empty($formData['propertyInformation']) && in_array("descIntermediaries", $formData['propertyInformation'])) {
            $xlsColumns[] = 'Uwarunkowania planistyczne';
        }
        if (isset($formData['propertyInformation']) && !empty($formData['propertyInformation']) && in_array("descLegalStatus", $formData['propertyInformation'])) {
            $xlsColumns[] = 'Opis stanu prawnego';
        }
        if (isset($formData['propertyInformation']) && !empty($formData['propertyInformation']) && in_array("descSalesAssumptions", $formData['propertyInformation'])) {
            $xlsColumns[] = 'Założenia sprzedaży';
        }
        if (isset($formData['propertyInformation']) && !empty($formData['propertyInformation']) && in_array("descNeighborhood", $formData['propertyInformation'])) {
            $xlsColumns[] = 'Opis w pobliżu';
        }
        if (isset($formData['propertyInformation']) && !empty($formData['propertyInformation']) && in_array("descPossibleUseProperty", $formData['propertyInformation'])) {
            $xlsColumns[] = 'Możliwe wykorzystanie nieruchomości';
        }
        if (isset($formData['propertyInformation']) && !empty($formData['propertyInformation']) && in_array("descIntermediarie", $formData['propertyInformation'])) {
            $xlsColumns[] = 'Informacje dla pośredników';
        }
        if (isset($formData['propertyInformation']) && !empty($formData['propertyInformation']) && in_array("descAdditional", $formData['propertyInformation'])) {
            $xlsColumns[] = 'Informacje dodatkowe';
        }
        if (isset($formData['propertyInformation']) && !empty($formData['propertyInformation']) && in_array("parCommunication", $formData['propertyInformation'])) {
            $xlsColumns[] = 'Strefa komunikacyjna';
        }
        if (isset($formData['propertyInformation']) && !empty($formData['propertyInformation']) && in_array("parGarage", $formData['propertyInformation'])) {
            $xlsColumns[] = 'Garaż';
        }
        if (isset($formData['propertyInformation']) && !empty($formData['propertyInformation']) && in_array("parUtilities", $formData['propertyInformation'])) {
            $xlsColumns[] = 'Media / uzbrojenie';
        }
        if (isset($formData['propertyInformation']) && !empty($formData['propertyInformation']) && in_array("paramFloor", $formData['propertyInformation'])) {
            $xlsColumns[] = 'Liczba pięter';
        }
        if (isset($formData['propertyInformation']) && !empty($formData['propertyInformation']) && in_array("parBasement", $formData['propertyInformation'])) {
            $xlsColumns[] = 'Piwnica';
        }
        if (isset($formData['advisorsSellers']) && !empty($formData['advisorsSellers']) && in_array("advisor", $formData['advisorsSellers'])) {
            $xlsColumns[] = 'Doradcy';
        }
        if (isset($formData['advisorsSellers']) && !empty($formData['advisorsSellers']) && in_array("seller", $formData['advisorsSellers'])) {
            $xlsColumns[] = 'Sprzedawcy';
        }
        if (isset($formData['pdf']) && !empty($formData['pdf']) && in_array("pdfDesc", $formData['pdf'])) {
            $xlsColumns[] = 'PDF Opis';
        }
        if (isset($formData['pdf']) && !empty($formData['pdf']) && in_array("pdfDescLocalization", $formData['pdf'])) {
            $xlsColumns[] = 'PDF Lokalizacja';
        }
        if (isset($formData['pdf']) && !empty($formData['pdf']) && in_array("pdfDescSalesAssumptions", $formData['pdf'])) {
            $xlsColumns[] = 'PDF Technika';
        }
        if (isset($formData['pdf']) && !empty($formData['pdf']) && in_array("pdfDescDestiny", $formData['pdf'])) {
            $xlsColumns[] = 'PDF Przeznaczenie';
        }

        if (isset($formData['other2']) && !empty($formData['other2']) && in_array("pdfNoPlot", $formData['other2'])) {
            $xlsColumns[] = 'Nr działki';
        }
        if (isset($formData['other2']) && !empty($formData['other2']) && in_array("pdfLandMortgageRegister", $formData['other2'])) {
            $xlsColumns[] = 'Nr KW';
        }

        if (isset($formData['pdf']) && !empty($formData['pdf']) && in_array("pdfDescPossibleUseProperty", $formData['pdf'])) {
            $xlsColumns[] = 'PDF Możliwe wykorzystanie nieruchomości';
        }
        if (isset($formData['pdf']) && !empty($formData['pdf']) && in_array("pdfDescIntermediaries", $formData['pdf'])) {
            $xlsColumns[] = 'PDF Dla pośredników';
        }
        if (isset($formData['other2']) && !empty($formData['other2']) && in_array("isActive", $formData['other2'])) {
            $xlsColumns[] = 'Aktywne';
        }
        if (isset($formData['other2']) && !empty($formData['other2']) && in_array("isSold", $formData['other2'])) {
            $xlsColumns[] = 'Sprzedane';
        }
        if (isset($formData['other2']) && !empty($formData['other2']) && in_array("isInPreparation", $formData['other2'])) {
            $xlsColumns[] = 'W przygotowaniu';
        }
        if (isset($formData['other2']) && !empty($formData['other2']) && in_array("buyNow", $formData['other2'])) {
            $xlsColumns[] = 'Kup teraz';
        }
        if (isset($formData['other2']) && !empty($formData['other2']) && in_array("isHit", $formData['other2'])) {
            $xlsColumns[] = 'Oferta Hit';
        }
        if (isset($formData['other2']) && !empty($formData['other2']) && in_array("typeClassified", $formData['other2'])) {
            $xlsColumns[] = 'Typ nieruchomości';
        }
        if (isset($formData['other2']) && !empty($formData['other2']) && in_array("link", $formData['other2'])) {
            $xlsColumns[] = 'Link';
        }
        if (isset($formData['other2']) && !empty($formData['other2']) && in_array("linkPdf", $formData['other2'])) {
            $xlsColumns[] = 'Link do karty obiektu (pdf)';
        }
        if (isset($formData['other2']) && !empty($formData['other2']) && in_array("labelNewPrice", $formData['other2'])) {
            $xlsColumns[] = 'Etykietka: nowa cena';
        }
        if (isset($formData['other2']) && !empty($formData['other2']) && in_array("makeOffer", $formData['other2'])) {
            $xlsColumns[] = 'Złóż ofertę';
        }
        if (isset($formData['other2']) && !empty($formData['other2']) && in_array("new", $formData['other2'])) {
            $xlsColumns[] = 'Nowość';
        }
        if (isset($formData['other2']) && !empty($formData['other2']) && in_array("isPreliminaryAgreement", $formData['other2'])) {
            $xlsColumns[] = 'Umowa przedwstępna';
        }
        if (isset($formData['other2']) && !empty($formData['other2']) && in_array("video", $formData['other2'])) {
            $xlsColumns[] = 'Film';
        }
        if (isset($formData['other2']) && !empty($formData['other2']) && in_array("exportOffer", $formData['other2'])) {
            $xlsColumns[] = 'Eksportuj ofertę';
        }
        if (isset($formData['other2']) && !empty($formData['other2']) && in_array("order", $formData['other2'])) {
            $xlsColumns[] = 'Kolejność';
        }
        if (isset($formData['other2']) && !empty($formData['other2']) && in_array("offerFile", $formData['other2'])) {
            $xlsColumns[] = 'Pliki oferty';
        }
        if (isset($formData['other2']) && !empty($formData['other2']) && in_array("clicksShowPhoneAdvisor", $formData['other2'])) {
            $xlsColumns[] = 'Kliknięcie w pokaż numer telefonu doradcy';
        }
        if (isset($formData['other2']) && !empty($formData['other2']) && in_array("clicksSendMsg", $formData['other2'])) {
            $xlsColumns[] = 'Kliknięcie wyślij wiadomość';
        }
        if (isset($formData['other2']) && !empty($formData['other2']) && in_array("clicksShowPhone", $formData['other2'])) {
            $xlsColumns[] = 'Kliknięcie w pokaż numer telefonu Biura Nieruchomości';
        }
        if (isset($formData['promotions']) && !empty($formData['promotions']) && in_array("discountPrice", $formData['promotions'])) {
            $xlsColumns[] = 'Nowa cena promocyjna';
        }
        if (isset($formData['promotions']) && !empty($formData['promotions']) && in_array("discountStart", $formData['promotions'])) {
            $xlsColumns[] = 'Start promocji';
        }
        if (isset($formData['promotions']) && !empty($formData['promotions']) && in_array("discountEnd", $formData['promotions'])) {
            $xlsColumns[] = 'Koniec promocji';
        }
        if (isset($formData['promotions']) && !empty($formData['promotions']) && in_array("isShowOldPrice", $formData['promotions'])) {
            $xlsColumns[] = 'Pokazuj starą cenę';
        }

        if (isset($formData['labels']) && !empty($formData['labels'])) {
            $labels = $this->labelRepository->findBy([], ['order' => 'ASC']);

            $_labels = [];
            /**
             * @var $label Label
             */

            foreach ($labels as $label) {
                foreach ($formData['labels'] as $show_label) {
                    /**
                     * @var $show_label Label
                     */
                    if ($show_label->getId() == $label->getId()) {
                        $xlsColumns[] = 'Etykieta: ' . $label->getName();
                        $xlsColumns[] = 'Etykieta data do: ' . $label->getName();
                        $xlsColumns[] = 'Etykieta kolejność: ' . $label->getName();
                        $_labels[$label->getId()]['act'] = 0;
                        $_labels[$label->getId()]['date'] = '';
                        $_labels[$label->getId()]['order'] = 0;
                    }
                }
            }
        }

        if (isset($formData['other2']) && !empty($formData['other2']) && in_array("photos", $formData['other2'])) {
            for ($i = 1; $i < 32; $i++) {
                $xlsColumns[] = "Zdjęcie_" . $i;
                $xlsColumns[] = "Alt_" . $i;
                $xlsColumns[] = "Tag_" . $i;
            }
        }

        /**
         * @var $classified Classified
         */
        foreach ($classifieds as $k => $classified) {
            if (isset($formData['labels']) && !empty($formData['labels'])) {
                //reset $labels
                foreach ($_labels as $labelID => $_label) {
                    $_labels[$labelID]['act'] = 0;
                    $_labels[$labelID]['date'] = '';
                    $_labels[$labelID]['order'] = '';
                }
                foreach ($classified->getClassifiedLabels() as $classifiedLabel) {
                    foreach ($formData['labels'] as $show_label) {
                        if ($show_label->getId() == $classifiedLabel->getLabel()->getId()) {
                            $_labels[$classifiedLabel->getLabel()->getId()]['act'] = 1;
                            $_labels[$classifiedLabel->getLabel()->getId()]['date'] = $classifiedLabel->getDateEnd() != null ? $classifiedLabel->getDateEnd()->format("Y-m-d") : '';
                            $_labels[$classifiedLabel->getLabel()->getId()]['order'] = $classified->getOrder();
                        }
                    }
                }
            }


            $xlsData[$k] = [];
            if (isset($formData['ids']) && !empty($formData['ids']) && in_array("id", $formData['ids'])) {
                $xlsData[$k][] = $classified->getId();//'id',
            }
            if (isset($formData['typeProperty']) && $formData['typeProperty'] == true) {
                $xlsData[$k][] = $classified->getTypeClassifiedText(); //'Typ oferty',
            }
            if (isset($formData['ids']) && !empty($formData['ids']) && in_array("tipi", $formData['ids'])) {
                $xlsData[$k][] = $classified->getTipi();//'TiPi',
            }
            if (isset($formData['ids']) && !empty($formData['ids']) && in_array("tipiAdditional", $formData['ids'])) {
                $xlsData[$k][] = $classified->getTipiAdditional(); //'TIPI dodatkowe',
            }
            if (isset($formData['address']) && !empty($formData['address']) && in_array("name", $formData['address'])) {
                $xlsData[$k][] = $classified->getName(); //'Nazwa',
            }
            if (isset($formData['address']) && !empty($formData['address']) && in_array("localizationStreet", $formData['address'])) {
                $xlsData[$k][] = $classified->getLocalizationStreet(); //'Ulica',
            }
            if (isset($formData['address']) && !empty($formData['address']) && in_array("localizationTown", $formData['address'])) {
                $xlsData[$k][] = $classified->getLocalizationTown(); //'Miejscowość',
            }
            if (isset($formData['address']) && !empty($formData['address']) && in_array("localizationVoivodeship", $formData['address'])) {
                $xlsData[$k][] = $classified->getLocalizationVoivodeshipText(); //'Województwo',
            }
            if (isset($formData['address']) && !empty($formData['address']) && in_array("localizationDistrict", $formData['address'])) {
                $xlsData[$k][] = $classified->getLocalizationDistrictText(); //'Powiat',
            }
            if (isset($formData['address']) && !empty($formData['address']) && in_array("localizationLong", $formData['address'])) {
                $xlsData[$k][] = $classified->getLocalizationLat(); //'Długość geograficzna',
            }
            if (isset($formData['address']) && !empty($formData['address']) && in_array("localizationLat", $formData['address'])) {
                $xlsData[$k][] = $classified->getLocalizationLong(); //'Szerokośc geograficzna',
            }
            if (isset($formData['price']) && !empty($formData['price']) && in_array("price", $formData['price'])) {
                $xlsData[$k][] = $classified->getPrice(); //'Cena',
            }
            if (isset($formData['price']) && !empty($formData['price']) && in_array("priceSqMeter", $formData['price'])) {
                $xlsData[$k][] = $classified->getPriceSqMeter(); //'Cena m2',
            }
            if (isset($formData['reservation']) && !empty($formData['reservation']) && in_array("reservationText", $formData['reservation'])) {
                $xlsData[$k][] = $classified->getReservationText(); //'Rezerwacja tekst',
            }
            if (isset($formData['reservation']) && !empty($formData['reservation']) && in_array("reservationTextEN", $formData['reservation'])) {
                $xlsData[$k][] = $classified->getReservationTextEN(); //'Rezerwacja tekst [EN]',
            }
            if (isset($formData['reservation']) && !empty($formData['reservation']) && in_array("reservationDateEnd", $formData['reservation'])) {
                $xlsData[$k][] = $classified->getReservationDateEnd(); //'Rezerwacja do',
            }
            if (isset($formData['area']) && !empty($formData['area']) && in_array("areaBuilding", $formData['area'])) {
                $xlsData[$k][] = $classified->getAreaBuilding(); //'Pow. Budynku',
            }
            if (isset($formData['area']) && !empty($formData['area']) && in_array("areaGround", $formData['area'])) {
                $xlsData[$k][] = $classified->getAreaGround(); //'Pow. Gruntu',
            }
            if (isset($formData['area']) && !empty($formData['area']) && in_array("areaRental", $formData['area'])) {
                $xlsData[$k][] = $classified->getAreaRental(); //'Pow. Wynajmu',
            }
            if (isset($formData['area']) && !empty($formData['area']) && in_array("areaRentalOffice", $formData['area'])) {
                $xlsData[$k][] = $classified->getAreaRentalOffice(); //'Pow. wynajmu - biurowa',
            }
            if (isset($formData['area']) && !empty($formData['area']) && in_array("areaRentalRetail", $formData['area'])) {
                $xlsData[$k][] = $classified->getAreaRentalRetail(); //'Pow. wynajmu - handlowa',
            }
            if (isset($formData['area']) && !empty($formData['area']) && in_array("areaRentalWarehouse", $formData['area'])) {
                $xlsData[$k][] = $classified->getAreaRentalWarehouse(); //'Pow. wynajmu - magazynowa',
            }
            if (isset($formData['area']) && !empty($formData['area']) && in_array("areaInBuildings", $formData['area'])) {
                $xlsData[$k][] = $classified->getAreaInBuildings(); //'Powierzchnia w budynkach',
            }
            if (isset($formData['area']) && !empty($formData['area']) && in_array("areaInGround", $formData['area'])) {
                $xlsData[$k][] = $classified->getAreaInGround(); //'Powierzchnia w gruncie',
            }
            if (isset($formData['area']) && !empty($formData['area']) && in_array("areaOnGround", $formData['area'])) {
                $xlsData[$k][] = $classified->getAreaOnGround(); //'Powierzchnia na gruncie',
            }
            if (isset($formData['area']) && !empty($formData['area']) && in_array("areaOnRoof", $formData['area'])) {
                $xlsData[$k][] = $classified->getAreaOnRoof(); //'Powierzchnia na dachu',
            }
            if (isset($formData['area']) && !empty($formData['area']) && in_array("areaOnFacade", $formData['area'])) {
                $xlsData[$k][] = $classified->getAreaOnFacade(); //'Powierzchnia na elewacji',
            }
            if (isset($formData['area']) && !empty($formData['area']) && in_array("areaInBuildingsPP", $formData['area'])) {
                $xlsData[$k][] = $classified->getAreaInBuildingsPP(); //'Powierzchnia w budynkach Poczta Polska',
            }
            if (isset($formData['other']) && !empty($formData['other']) && in_array("lawTitle", $formData['other'])) {
                $xlsData[$k][] = $classified->getLawTitle(); //'Tytuł prawny',
            }
            if (isset($formData['other']) && !empty($formData['other']) && in_array("conservatorProtection", $formData['other'])) {
                $xlsData[$k][] = $classified->getConservatorProtection(); //'Ochrona konserwatora',
            }
            if (isset($formData['other']) && !empty($formData['other']) && in_array("mpzp", $formData['other'])) {
                $xlsData[$k][] = $classified->getMpzpName(); //'MPZP',
            }
            if (isset($formData['publication']) && !empty($formData['publication']) && in_array("dateStart", $formData['publication'])) {
                $xlsData[$k][] = $classified->getDateStart(); //
            }
            if (isset($formData['publication']) && !empty($formData['publication']) && in_array("dateEnd", $formData['publication'])) {
                $xlsData[$k][] = $classified->getDateEnd(); //'Data zakończenia publikacji',
            }
            if (isset($formData['publication']) && !empty($formData['publication']) && in_array("planDate", $formData['publication'])) {
                $xlsData[$k][] = $classified->getPlanDate(); //'Planowana data przedstawienia oferty',
            }
            if (isset($formData['desc']) && !empty($formData['desc']) && in_array("descShort", $formData['desc'])) {
                $xlsData[$k][] = $this->clearTags(html_entity_decode($classified->getDescShort())); //'Opis kótki',
            }
            if (isset($formData['desc']) && !empty($formData['desc']) && in_array("descShortEN", $formData['desc'])) {
                $xlsData[$k][] = $this->clearTags(html_entity_decode($classified->getDescShortEN())); //'Opis kótki [EN]',
            }
            if (isset($formData['desc']) && !empty($formData['desc']) && in_array("desc", $formData['desc'])) {
                $xlsData[$k][] = $this->clearTags(html_entity_decode($classified->getDesc())); //'Opis ogólny',
            }
            if (isset($formData['desc']) && !empty($formData['desc']) && in_array("descEN", $formData['desc'])) {
                $xlsData[$k][] = $this->clearTags(html_entity_decode($classified->getDescEN())); //'Opis ogólny [EN]',
            }
            if (isset($formData['desc']) && !empty($formData['desc']) && in_array("descLocalization", $formData['desc'])) {
                $xlsData[$k][] = $this->clearTags(html_entity_decode($classified->getDescLocalization())); //'Opis lokalizacji',
            }
            if (isset($formData['desc']) && !empty($formData['desc']) && in_array("descLocalizationEN", $formData['desc'])) {
                $xlsData[$k][] = $this->clearTags(html_entity_decode($classified->getDescLocalizationEN())); //'Opis lokalizacji [EN]',
            }
            if (isset($formData['propertyInformation']) && !empty($formData['propertyInformation']) && in_array("descIntermediaries", $formData['propertyInformation'])) {
                $xlsData[$k][] = $this->clearTags(html_entity_decode($classified->getDescIntermediaries())); //'Uwarunkowania planistyczne',
            }
            if (isset($formData['propertyInformation']) && !empty($formData['propertyInformation']) && in_array("descLegalStatus", $formData['propertyInformation'])) {
                $xlsData[$k][] = $this->clearTags(html_entity_decode($classified->getDescLegalStatus())); //'Opis stanu prawnego',
            }
            if (isset($formData['propertyInformation']) && !empty($formData['propertyInformation']) && in_array("descSalesAssumptions", $formData['propertyInformation'])) {
                $xlsData[$k][] = $this->clearTags(html_entity_decode($classified->getDescSalesAssumptions())); //'Założenia sprzedaży',
            }
            if (isset($formData['propertyInformation']) && !empty($formData['propertyInformation']) && in_array("descNeighborhood", $formData['propertyInformation'])) {
                $xlsData[$k][] = $this->clearTags(html_entity_decode($classified->getDescNeighborhood())); //'Opis w pobliżu',
            }
            if (isset($formData['propertyInformation']) && !empty($formData['propertyInformation']) && in_array("descPossibleUseProperty", $formData['propertyInformation'])) {
                $xlsData[$k][] = $this->clearTags(html_entity_decode($classified->getDescPossibleUseProperty())); //'Możliwe wykorzystanie nieruchomości',
            }
            if (isset($formData['propertyInformation']) && !empty($formData['propertyInformation']) && in_array("descIntermediarie", $formData['propertyInformation'])) {
                $xlsData[$k][] = $this->clearTags(html_entity_decode($classified->getDescIntermediaries())); //'Informacje dla pośredników',
            }
            if (isset($formData['propertyInformation']) && !empty($formData['propertyInformation']) && in_array("descAdditional", $formData['propertyInformation'])) {
                $xlsData[$k][] = $this->clearTags(html_entity_decode($classified->getDescAdditional())); //'Informacje dodatkowe',
            }
            if (isset($formData['propertyInformation']) && !empty($formData['propertyInformation']) && in_array("parCommunication", $formData['propertyInformation'])) {
                $xlsData[$k][] = $classified->getParCommunicationList(); //'Strefa komunikacyjna',
            }
            if (isset($formData['propertyInformation']) && !empty($formData['propertyInformation']) && in_array("parGarage", $formData['propertyInformation'])) {
                $xlsData[$k][] = $classified->getParGarageText(); //'Garaż',
            }
            if (isset($formData['propertyInformation']) && !empty($formData['propertyInformation']) && in_array("parUtilities", $formData['propertyInformation'])) {
                $xlsData[$k][] = $classified->getParUtilitiesList(); //'Media / uzbrojenie',
            }
            if (isset($formData['propertyInformation']) && !empty($formData['propertyInformation']) && in_array("paramFloor", $formData['propertyInformation'])) {
                $xlsData[$k][] = $classified->getParamFloor(); //'Liczba pięter',
            }
            if (isset($formData['propertyInformation']) && !empty($formData['propertyInformation']) && in_array("parBasement", $formData['propertyInformation'])) {
                $xlsData[$k][] = $classified->getParBasementText(); //'Piwnica',
            }
            if (isset($formData['advisorsSellers']) && !empty($formData['advisorsSellers']) && in_array("advisor", $formData['advisorsSellers'])) {
                $xlsData[$k][] = $classified->getAdvisorList(); //'Doradcy',
            }
            if (isset($formData['advisorsSellers']) && !empty($formData['advisorsSellers']) && in_array("seller", $formData['advisorsSellers'])) {
                $xlsData[$k][] = $classified->getSellerList(); //'Sprzedawcy',
            }
            if (isset($formData['pdf']) && !empty($formData['pdf']) && in_array("pdfDesc", $formData['pdf'])) {
                $xlsData[$k][] = $this->clearTags(html_entity_decode($classified->getPdfDesc())); //'PDF Opis',
            }
            if (isset($formData['pdf']) && !empty($formData['pdf']) && in_array("pdfDescLocalization", $formData['pdf'])) {
                $xlsData[$k][] = $this->clearTags(html_entity_decode($classified->getPdfDescLocalization())); //'PDF Lokalizacja',
            }
            if (isset($formData['pdf']) && !empty($formData['pdf']) && in_array("pdfDescSalesAssumptions", $formData['pdf'])) {
                $xlsData[$k][] = $this->clearTags(html_entity_decode($classified->getPdfDescSalesAssumptions())); //'PDF Technika',
            }
            if (isset($formData['pdf']) && !empty($formData['pdf']) && in_array("pdfDescDestiny", $formData['pdf'])) {
                $xlsData[$k][] = $this->clearTags(html_entity_decode($classified->getPdfDescDestiny())); //'PDF Przeznaczenie',
            }

            if (isset($formData['other2']) && !empty($formData['other2']) && in_array("pdfNoPlot", $formData['other2'])) {
                $xlsData[$k][] = $this->clearTags(html_entity_decode($classified->getPdfNoPlot())); //'Nr działki',
            }
            if (isset($formData['other2']) && !empty($formData['other2']) && in_array("pdfLandMortgageRegister", $formData['other2'])) {
                $xlsData[$k][] = $this->clearTags(html_entity_decode($classified->getPdfLandMortgageRegister())); //'Nr KW',
            }

            if (isset($formData['pdf']) && !empty($formData['pdf']) && in_array("pdfDescPossibleUseProperty", $formData['pdf'])) {
                $xlsData[$k][] = $this->clearTags(html_entity_decode($classified->getPdfDescPossibleUseProperty())); //'PDF Możliwe wykorzystanie nieruchomości',
            }
            if (isset($formData['pdf']) && !empty($formData['pdf']) && in_array("pdfDescIntermediaries", $formData['pdf'])) {
                $xlsData[$k][] = $this->clearTags(html_entity_decode($classified->getPdfDescIntermediaries())); //'PDF Dla pośredników',
            }
            if (isset($formData['other2']) && !empty($formData['other2']) && in_array("isActive", $formData['other2'])) {
                $xlsData[$k][] = $classified->getIsActive() ? "Tak" : "Nie";//'Aktywne',
            }
            if (isset($formData['other2']) && !empty($formData['other2']) && in_array("isSold", $formData['other2'])) {
                $xlsData[$k][] = $classified->getIsSold() ? "Tak" : "Nie";//'Sprzedane',
            }
            if (isset($formData['other2']) && !empty($formData['other2']) && in_array("isInPreparation", $formData['other2'])) {
                $xlsData[$k][] = $classified->getIsInPreparation() ? "Tak" : "Nie";//'W przygotowaniu',
            }
            if (isset($formData['other2']) && !empty($formData['other2']) && in_array("buyNow", $formData['other2'])) {
                $xlsData[$k][] = $classified->getBuyNowPrice(); //'Kup teraz',
            }
            if (isset($formData['other2']) && !empty($formData['other2']) && in_array("isHit", $formData['other2'])) {
                $xlsData[$k][] = $classified->getIsHit() ? "Tak" : "Nie";//'Oferta Hit',
            }
            if (isset($formData['other2']) && !empty($formData['other2']) && in_array("typeClassified", $formData['other2'])) {
                $xlsData[$k][] = $classified->getTypePropertyText(); //'Typ nieruchomości',
            }
            if (isset($formData['other2']) && !empty($formData['other2']) && in_array("link", $formData['other2'])) {
                $xlsData[$k][] = $classified->getClassifiedLink(); //'Link',
            }
            if (isset($formData['other2']) && !empty($formData['other2']) && in_array("linkPdf", $formData['other2'])) {
                $xlsData[$k][] = $classified->getClassifiedLinkPdf(); //'Link do karty obiektu (pdf)',
            }
            if (isset($formData['other2']) && !empty($formData['other2']) && in_array("labelNewPrice", $formData['other2'])) {
                $xlsData[$k][] = $classified->getIsNewPrice() ? "Tak" : "Nie";//'Nowa cena',
            }
            if (isset($formData['other2']) && !empty($formData['other2']) && in_array("makeOffer", $formData['other2'])) {
                $xlsData[$k][] = $classified->getIsMakeOffer() ? "Tak" : "Nie";//'Złóż ofertę',
            }
            if (isset($formData['other2']) && !empty($formData['other2']) && in_array("new", $formData['other2'])) {
                $xlsData[$k][] = $classified->getIsNew() ? "Tak" : "Nie";//'Nowość',
            }
            if (isset($formData['other2']) && !empty($formData['other2']) && in_array("isPreliminaryAgreement", $formData['other2'])) {
                $xlsData[$k][] = $classified->getIsPreliminaryAgreement() ? "Tak" : "Nie";//'Umowa przedwstępna',
            }
            if (isset($formData['other2']) && !empty($formData['other2']) && in_array("video", $formData['other2'])) {
                $xlsData[$k][] = $classified->getVideoUrl(); //'Film',
            }
            if (isset($formData['other2']) && !empty($formData['other2']) && in_array("exportOffer", $formData['other2'])) {
                $xlsData[$k][] = $classified->getIsExported(); //'Eksportuj ofertę',
            }
            if (isset($formData['other2']) && !empty($formData['other2']) && in_array("order", $formData['other2'])) {
                $xlsData[$k][] = $classified->getOrder(); //'Kolejność',
            }
            if (isset($formData['other2']) && !empty($formData['other2']) && in_array("offerFile", $formData['other2'])) {
                $xlsData[$k][] = $this->getClassifiedFilesList($classified); //'Pliki oferty',
            }
            if (isset($formData['other2']) && !empty($formData['other2']) && in_array("clicksShowPhoneAdvisor", $formData['other2'])) {
                $xlsData[$k][] = $this->getClassifiedStats($classified, 1); //'Kliknięcie w pokaż numer telefonu doradcy',
            }
            if (isset($formData['other2']) && !empty($formData['other2']) && in_array("clicksSendMsg", $formData['other2'])) {
                $xlsData[$k][] = $this->getClassifiedStats($classified, 2); //'Kliknięcie wyślij wiadomość',
            }
            if (isset($formData['other2']) && !empty($formData['other2']) && in_array("clicksShowPhone", $formData['other2'])) {
                $xlsData[$k][] = $this->getClassifiedStats($classified, 3); //'Kliknięcie w pokaż numer telefonu Biura Nieruchomości',
            }
            if (isset($formData['promotions']) && !empty($formData['promotions']) && in_array("discountPrice", $formData['promotions'])) {
                $xlsData[$k][] = $classified->getDiscountPrice(); //'Nowa cena',
            }
            if (isset($formData['promotions']) && !empty($formData['promotions']) && in_array("discountStart", $formData['promotions'])) {
                $xlsData[$k][] = $classified->getDiscountStart(); //'Start promocji',
            }
            if (isset($formData['promotions']) && !empty($formData['promotions']) && in_array("discountEnd", $formData['promotions'])) {
                $xlsData[$k][] = $classified->getDiscountEnd(); //'Koniec promocji',
            }
            if (isset($formData['promotions']) && !empty($formData['promotions']) && in_array("isShowOldPrice", $formData['promotions'])) {
                $xlsData[$k][] = $classified->isShowOldPrice();//'Pokazuj starą cenę'
            }
//            ];

            if (!empty($_labels)) {
                foreach ($_labels as $label) {
                    $xlsData[$k][] = $label['act'];
                    $xlsData[$k][] = $label['date'];
                    $xlsData[$k][] = $label['order'];
                }
            }

            if (isset($formData['other2']) && !empty($formData['other2']) && in_array("photos", $formData['other2'])) {
                $i = 1;
                foreach ($classified->getClassifiedPhotos() as $photo) {
                    $xlsData[$k][] = $this->param->get("app.site_url_api") . '/' . $photo->getFilePath();
                    $xlsData[$k][] = $photo->getTitle();
                    $xlsData[$k][] = $photo->getTags();
                }
            }
        }

        $fileName = 'raport_' . date("Y-m-d") . '';

        $this->saveXls($fileName, $xlsColumns, $xlsData, 'Raport', false);


    }

    public function clearTags($content)
    {
//        $content = str_replace("</p>", "\n", $content);
//        $content = str_replace("</li>", "\n", $content);

        $content = str_replace("&oacute;", "ó", $content);
        $content = str_replace("&nbsp;", " ", $content);
        $content = str_replace("&ndash;", "-", $content);
        $content = str_replace("&bdquo;", "„", $content);
        $content = str_replace("&rdquo;", "”", $content);
        $content = str_replace("&sup2;", "²", $content);
        $content = str_replace("&bull;", "•", $content);
        $content = str_replace("&hellip;", "…", $content);
        $content = str_replace("&minus;", "-", $content);

        $content = str_replace(["<br/>", "<br>", "<br />"], "\n", $content);
        $content = str_replace("<li>", "• ", $content);
        $content = strip_tags($content);
        return $content;
    }

    public function saveXls($fileName, $xlsColumns, $xlsData, $fileTitle = 'Raport', $debug = false)
    {
        $spreadsheet = new Spreadsheet();

        /* @var $sheet \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Worksheet */
        $sheet = $spreadsheet->getActiveSheet();

        if (!empty($xlsColumns)) {
            $c = 1;
            foreach ($xlsColumns as $xlsColumn) {
                $sheet->setCellValueByColumnAndRow($c, 1, $xlsColumn);
                $c++;
            }
        }


        if (!empty($xlsData)) {
            foreach ($xlsData as $r => $xlsRowData) {
                $c = 1;
                foreach ($xlsRowData as $data) {
                    $sheet->setCellValueByColumnAndRow($c, $r + 2, $data);
                    $c++;
                }

            }
        }

        $sheet->setTitle($fileTitle);

        //Debug
        if ($debug) {
            $writer = new Html($spreadsheet);
            $writer->save('php://output');
            die();
        }

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Cache-Control: max-age=0');
        header('Content-Disposition: attachment;filename="' . $fileName . '.xlsx"');

        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Allow-Headers: accept, content-type');
        header('Access-Control-Allow-Methods: POST, OPTIONS');
        header('Access-Control-Allow-Origin: *');


        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
    }


    public function getClassifiedFilesList(Classified $classified)
    {
        $files = $this->classifiedFileRepository->findBy(['classified' => $classified]);
        $classifiedFiles = [];
        if (!empty($files)) {
            /** @var ClassifiedFile $file */
            foreach ($files as $file) {
                $classifiedFiles[] = $this->param->get("app.site_url_api") . '/' . $this->param->get("app.classified_file_dir") . '/' . $classified->getId() . '/' . $file->getFileName();
            }
        }
        return implode(",\n", $classifiedFiles);
    }

    public function getClassifiedStats(Classified $classified, $status)
    {
        return $this->statsClassifiedRepository->countStatsClassifiedByStatus($classified, $status);
    }

    public function fixDTOUrl(ClassifiedSingleOutputDTO $classifiedDTO)
    {
        $classifiedDTO->setDesc($this->fixUrl($classifiedDTO->getDesc()));
        $classifiedDTO->setDescEN($this->fixUrl($classifiedDTO->getDescEN()));
        $classifiedDTO->setDescLocalization($this->fixUrl($classifiedDTO->getDescLocalization()));
        $classifiedDTO->setDescLocalizationEN($this->fixUrl($classifiedDTO->getDescLocalizationEN()));
        $classifiedDTO->setDescDestiny($this->fixUrl($classifiedDTO->getDescDestiny()));
        $classifiedDTO->setDescDestinyEN($this->fixUrl($classifiedDTO->getDescDestinyEN()));
        $classifiedDTO->setDescSalesAssumptions($this->fixUrl($classifiedDTO->getDescSalesAssumptions()));
        $classifiedDTO->setDescSalesAssumptionsEN($this->fixUrl($classifiedDTO->getDescSalesAssumptionsEN()));
        $classifiedDTO->setDescAdditional($this->fixUrl($classifiedDTO->getDescAdditional()));
        $classifiedDTO->setDescAdditionalEN($this->fixUrl($classifiedDTO->getDescAdditionalEN()));
        $classifiedDTO->setDescNeighborhood($this->fixUrl($classifiedDTO->getDescNeighborhood()));
        $classifiedDTO->setDescNeighborhoodEN($this->fixUrl($classifiedDTO->getDescNeighborhoodEN()));
        $classifiedDTO->setDescLegalStatus($this->fixUrl($classifiedDTO->getDescLegalStatus()));
        $classifiedDTO->setDescLegalStatusEN($this->fixUrl($classifiedDTO->getDescLegalStatusEN()));
        $classifiedDTO->setDescShort($this->fixUrl($classifiedDTO->getDescShort()));
        $classifiedDTO->setDescShortEN($this->fixUrl($classifiedDTO->getDescShortEN()));
        $classifiedDTO->setDescPossibleUseProperty($this->fixUrl($classifiedDTO->getDescPossibleUseProperty()));
        $classifiedDTO->setDescPossibleUsePropertyEN($this->fixUrl($classifiedDTO->getDescPossibleUsePropertyEN()));
        $classifiedDTO->setDescIntermediaries($this->fixUrl($classifiedDTO->getDescIntermediaries()));
        $classifiedDTO->setDescIntermediariesEN($this->fixUrl($classifiedDTO->getDescIntermediariesEN()));

        return $classifiedDTO;
    }

    public function updateDateEnd()
    {
        $i = 0;
        $offset = 20;

        do {
            echo "\n\n";
            echo 'Paczka: ' . $i . "\n";
            $classifieds = $this->classifiedRepository->findBy([], ["id" => "DESC"], $offset, $i * $offset);

            /**
             * @var $classified Classified
             */
            foreach ($classifieds as $classified) {
                echo "Update: " . $classified->getName() . "\n";

                if ($classified->getDateEnd()) {
                    $dateEnd = clone $classified->getDateEnd();
                    $classified->setDateEnd($dateEnd->setTime(23, 59, 59));
                }
                if ($classified->getDiscountEnd()) {
                    $discountEnd = clone $classified->getDiscountEnd();
                    $classified->setDiscountEnd($discountEnd->setTime(23, 59, 59));
                }
            }
            $this->em->flush();
            $i++;
        } while ($classifieds);
    }

    private function fixUrl($string)
    {
        $siteUrl = $this->param->get("app.site_url_api");
        $string = str_replace(["src=\"media"], ["src=\"{$siteUrl}/media"], $string);
        $string = str_replace(["src=\"/useruploads"], ["src=\"{$siteUrl}/useruploads"], $string);
        $string = str_replace(["src=\"useruploads"], ["src=\"{$siteUrl}/useruploads"], $string);
        return $string;
    }

}