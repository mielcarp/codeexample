<?php


namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ReCaptcha extends Constraint
{
    public $message = 'bad_recaptcha: {{ string }}';
    public $disable = false;
}