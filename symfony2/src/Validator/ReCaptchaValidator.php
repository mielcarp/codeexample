<?php


namespace App\Validator;

use App\Service\RecaptchaService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

/**
 * @Annotation
 */
class ReCaptchaValidator extends ConstraintValidator
{
    /**
     * @var ParameterBagInterface
     */
    private $param;
    private $recaptchaService;

    public function __construct(ParameterBagInterface $param, RecaptchaService $recaptchaService)
    {
        $this->param = $param;
        $this->recaptchaService = $recaptchaService;
    }

    public function validate($value, Constraint $constraint)
    {
        /**
         * @var $constraint ReCaptcha
         */
        if (!$constraint instanceof ReCaptcha) {
            throw new UnexpectedTypeException($constraint, ReCaptcha::class);
        }

        if (null === $value || '' === $value) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ string }}', 'empty token')
                ->addViolation();
            return;
        }

        if (!is_string($value)) {
            throw new UnexpectedValueException($value, 'string');
        }

        if($constraint->disable){
            return;
        }

        $response = $this->recaptchaService->check($value, true);

        if (empty($response) || !isset($response['success']) || $response['success'] == false) {
            $errorMesg = implode(', ' , $response['error-codes']);
            // the argument must be a string or an object implementing __toString()
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ string }}', $errorMesg)
                ->addViolation();
        }
    }
}